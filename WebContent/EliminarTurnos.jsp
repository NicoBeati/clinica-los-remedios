<%@ page import="java.util.List" %>
<%@ page import="entidad.Turnos" %> 

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Ver/Eliminar Turnos</title>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
      <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
</head>
<style type="text/css">	
        .tablaGrilla {
            width: 100%;
        }
        .columnaLateral_tablaGrilla {
            width: 20%;
        }
        .columnaCentral_tablaGrilla {
            width: 30%;
        }
</style>
<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  
	
    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesi�n </a> 
 </div>
 
 
 <a href="HomeAdmin.jsp" role="button" class="btn btn-primary ms-5 mt-3 mb-2"> Volver a Home </a> 
 
 <div class=" px-5  p-5 rounded-1  bg-light  mx-auto" style="width: 90%;" >
 
		<p class="fs-3 mx-auto mb-5 text-center">Eliminar turno</p>


	<table id="tableTurnos" class="display table" >
	    <thead class="mt-5  table-info" style="width: 100% !important; ">
	        <tr>
	        	<th>Turno</th>
	            <th>DNI del paciente</th>
	            <th>Nombre Especialista</th>
	            <th>Especialidad</th>
	            <th>Fecha de Turno</th>
	            <th>Horario</th>
	            <th>Eliminar</th>
	        </tr>
	    </thead>
	    <tbody>
            <% 
            List<Turnos> listaTurnos = (List<Turnos>) request.getAttribute("listaTurnos");
            if (listaTurnos != null) {
                for (Turnos turno : listaTurnos) {
                	if(!turno.getDni_Paciente_Turnos().equals("Sin pacientes")){

            %>
            <tr>
             <form action="ServletEliminarTurnos" method="post" onsubmit="return confirmacion()" >
                <td><%= turno.getCod_Turnos() %><input type="hidden" value=<%= turno.getCod_Turnos() %> name="Cod_Turno"></td>
                <td><%= turno.getDni_Paciente_Turnos() %></td>
                <td><%= turno.getDni_Especialista_Turnos() %></td>
                <td><%= turno.getCod_Especialidad_Turnos() %></td>
                <td><%= turno.getFecha_Turnos() %></td>
                <td><%= turno.getHorario_Turnos() %></td>
                <td><input type="submit" value="Eliminar" name="btnEliminarTurnos" class='btn btn-danger'></td>
            </form>
            </tr>
            <% 
                	}
                }
            }
            %>
        </tbody>
	</table>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<%	
boolean agregado = false;
Object agregoAttr = request.getAttribute("Agrego");
if (agregoAttr != null) {
    agregado = (boolean) agregoAttr;
}
 %>

<%
if (request.getParameter("btnEliminarTurnos") != null) {
    if (agregado) {
%>
<script>
		Swal.fire({
			  
			  icon: 'success',
			  title: 'Turno eliminado con �xito',
			  showConfirmButton: false,
			  timer: 1500
			})		 
		</script>

    
<%
    } else {
%>
<script>
		Swal.fire({
			  
			  icon: 'error',
			  title: 'Error al eliminar el turno, corrobore que los datos no sean erroneos.',
			  showConfirmButton: false,
			  timer: 1500
			})		 
		</script>


  
<%
    }
}
%>
	<br>
</div>

<script type="text/javascript">
    $(document).ready(function(){
    	$('#tableTurnos').DataTable({
    		 language: {
                 processing: "Procesando...",
                 lengthMenu: "Mostrar _MENU_ registros",
                 zeroRecords: "No se encontraron resultados",
                 emptyTable: "Ning�n dato disponible en esta tabla",
                 info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                 infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                 infoFiltered: "(filtrado de un total de _MAX_ registros)",
                 infoPostFix: "",
                 search: "Buscar:",
                 thousands: ",",
                 loadingRecords: "Cargando...",
                 paginate: {
                     first: "Primero",
                     last: "�ltimo",
                     next: "Siguiente",
                     previous: "Anterior"
                 },
                 aria: {
                     sortAscending: ": Activar para ordenar la columna de manera ascendente",
                     sortDescending: ": Activar para ordenar la columna de manera descendente"
                 }
             }
    	});
    	});</script>

<script>
function confirmacion() {
    return confirm("¿Está seguro que desea realizar esta acción?");
}
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>

</body>
</html>