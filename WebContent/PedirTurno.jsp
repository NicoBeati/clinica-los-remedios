<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="entidad.Especialistas" %>
<%@ page import="entidad.Turnos" %>
<%@ page import="entidad.Pacientes"%>
<%@ page import="entidad.Especialidad"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
  <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
  <script src='datepicker-es.js' type='text/javascript'></script>
   <link href='jquery-ui.min.css' rel='stylesheet' type='text/css'>
  <script src='jquery-ui.min.js' type='text/javascript'></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
  <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  
<title>Pedir Turno</title>
<style type="text/css">	
        .tablaGrilla {
            width: 100%;
        }
        .columnaLateral_tablaGrilla {
            width: 25%;
        }
        .columnaCentral_tablaGrilla {
            width: 25%;
        }
         
</style>
</head>
<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  
	
    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesión </a> 
 </div>
 
 <a href="HomeAdmin.jsp" role="button" class="btn btn-primary ms-5 mt-3 mb-2"> Volver a Home </a> 
<form action="ServletsPedirTurno" method="get" id="formpedirTurno"  onsubmit="return confirmacion()" >
	<div class=" px-5  p-5 rounded-1  bg-light  mx-auto" style="width: 90%;" >
	<p class="fs-3 mx-auto mb-5 text-center">Pedir turno</p>
	
	<%

boolean agregado = false;
Object agregoAttr = request.getAttribute("Agrego");
if (agregoAttr != null) {
    agregado = (boolean) agregoAttr;
}
 %>

<%
if (request.getParameter("btnguardar") != null) {
    if (agregado) {
%>
<script>
		Swal.fire({
			  
			  icon: 'success',
			  title: 'Turno agregado con �xito',
			  showConfirmButton: false,
			  timer: 2000
			})		 
		</script>
    
<%
    } else {
%>
<script>
		Swal.fire({
			  
			  icon: 'error',
			  title: 'Error al agregar el turno, corrobore que los datos no sean erroneos.',
			  showConfirmButton: false,
			  timer: 2000
			})		 
		</script>
    

  
<%
    }
}
%>
	
		<table class="display table" id="TablaTurnos">
		<thead class="mt-5  table-info" style="width: 100% !important; ">
		<tr>
			<th  hidden></</th>
			<th>Nombre Especialista</th>
			<th>Especialidad</th>
			<th hidden></th>
			<th>Dni paciente</th>
			<th hidden></th>
			<th>Asistencia</th>
			<th>Fecha</th>
			<th>Dia</th>
			<th>Horario</th>
			<th>Modificar</th>
		</tr>
		</thead>
		<tbody>
		            <% 
            List<Turnos> listaTurnos= (List<Turnos>) request.getAttribute("turnosListLIBRES");
		        if (listaTurnos != null) {
                for (Turnos turnos : listaTurnos) {
                if(turnos.getAsistencia_Turnos()==1){
                	 
            %>
		<tr >
			<td hidden><input type="text" class="cod" value="<%=turnos.getCod_Turnos()%>"></td>
			<td class="vertical-align"><%=turnos.getDni_Especialista_Turnos() %></td>
			<td ><%=turnos.getCod_Especialidad_Turnos() %></td>
			
			<td hidden ><%=turnos.getDni_Paciente_Turnos() %></td>
			<td ><input type="text" class="dni form-control"></td>
			<td hidden ><%=turnos.getAsistencia_Turnos() %></td>
			<td ><select type="text" class="Asistencia form-select">
				<option>--SELECCIONAR--</option>
				<option value="0">Ocupado</option>
			</select></td>
			<td ><%=turnos.getFecha_Turnos() %></td>
			<td ><%=turnos.getDia_Turnos() %></td>
			<td ><%=turnos.getHorario_Turnos() %></td>
			<td></td>
			</tr>
		 <% 
		 }
                }
            }
            %>
		</tbody>
			</table>
			</div>

	</form>
	<script type="text/javascript"> 
    $(document).ready(function(){
    	var table=$('#TablaTurnos').DataTable({
    	    "columnDefs": [
    	        {
    	          "targets": -1, // Última columna
    	          "data": null,
    	          "defaultContent": "<button type='submit' class='btn btn-success'>Guardar Turnos</button>",
    	          "orderable": false,
    	          "searchable": false
    	        }
    	      ],
    	      language: {
                  processing: "Procesando...",
                  lengthMenu: "Mostrar _MENU_ registros",
                  zeroRecords: "No se encontraron resultados",
                  emptyTable: "Ningún dato disponible en esta tabla",
                  info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                  infoFiltered: "(filtrado de un total de _MAX_ registros)",
                  infoPostFix: "",
                  search: "Buscar:",
                  thousands: ",",
                  loadingRecords: "Cargando...",
                  paginate: {
                      first: "Primero",
                      last: "Último",
                      next: "Siguiente",
                      previous: "Anterior"
                  },
                  aria: {
                      sortAscending: ": Activar para ordenar la columna de manera ascendente",
                      sortDescending: ": Activar para ordenar la columna de manera descendente"
                  }
              }



    	    });
    	$('#formpedirTurno').submit(function(event) {
    	    // Evitar el envío del formulario por defecto
    	    event.preventDefault();
    	    var fila = $(document.activeElement).parents('tr');
    		var dni = fila.find('input.dni').val();
    		var asistencia=fila.find('select.Asistencia').val();
    		var cod = fila.find('input.cod').val();
            // Validar que se haya seleccionado una opción válida
            if (asistencia === '--SELECCIONAR--') {
                alert('Por favor, seleccione una opción de asistencia.');
                return;
            }
    		
    	    // Agregar parámetros adicionales al formulario
    	    var parametrosAdicionales = {
    	    	  dni_paciente:dni,
   	              Asistencia_turno:asistencia,
   	              Id_turno:cod,
   	              btnguardar:1
    	    };

    	    // Desactivar la escucha del evento submit
    	    $(this).off('submit');

    	    // Agregar los parámetros como campos ocultos al formulario
    	    $.each(parametrosAdicionales, function(key, value) {
    	      $('#TablaTurnos').append($('<input>').attr({
    	        type: 'hidden',
    	        name: key,
    	        value: value
    	      }));
    	    });

    	    // Enviar el formulario
    	    this.submit();
    	  });
    	
    	  });
 
    </script>
	


<script>
function confirmacion() {
    return confirm("¿Está seguro que desea realizar esta acción?");
}
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>