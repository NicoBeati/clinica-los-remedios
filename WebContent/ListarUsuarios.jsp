<%@ page import="java.util.List" %>
<%@ page import="entidad.Usuarios" %> 

<!DOCTYPE html>
<html>
<head>
    <title>Tabla de Usuarios</title>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
    <style type="text/css">	
        .tablaGrilla {
            width: 100%;
        }
        .columnaLateral_tablaGrilla {
            width: 25%;
        }
        .columnaCentral_tablaGrilla {
            width: 25%;
        }
         
</style>
</head>
<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  
	
    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesi�n </a> 
 </div>
	<a href="HomeAdmin.jsp" role="button" class="btn btn-primary ms-5 mt-3 mb-2"> Volver a Home </a>
	
	<div class=" px-5  p-5 rounded-1  bg-light  mx-auto" style="width: 80%" >
	<p class="fs-3 mx-auto mb-5 text-center">Usuarios</p>
    <table id="usuarios" class="table  ">
        <thead class="mt-5  table-info" style="width: 100% !important; ">
            <tr class="">
                <th>DNI</th>
                <th>Nombre de Usuario</th>
                <th>Email</th>
                <th>Tipo de Usuario</th>
            </tr>
        </thead>
        <tbody>
            <% 
            List<Usuarios> listaUsuarios = (List<Usuarios>) request.getAttribute("listaUsuarios");
            if (listaUsuarios != null) {
                for (Usuarios usuario : listaUsuarios) {
            %>
            <tr>
                <td><%= usuario.getDni() %></td>
                <td><%= usuario.getUsuario() %></td>
                <td><%= usuario.getEmail_Usuarios() %></td>
                <td><%= usuario.getTipo_Usuarios() %></td>
            </tr>
            <% 
                }
            }
            %>
        </tbody>
    </table>
    </div>
    <script type="text/javascript">
    $(document).ready(function(){
    	$('#usuarios').DataTable({
    		language: {
                processing: "Procesando...",
                lengthMenu: "Mostrar _MENU_ registros",
                zeroRecords: "No se encontraron resultados",
                emptyTable: "Ning�n dato disponible en esta tabla",
                info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                infoFiltered: "(filtrado de un total de _MAX_ registros)",
                infoPostFix: "",
                search: "Buscar:",
                thousands: ",",
                loadingRecords: "Cargando...",
                paginate: {
                    first: "Primero",
                    last: "�ltimo",
                    next: "Siguiente",
                    previous: "Anterior"
                },
                aria: {
                    sortAscending: ": Activar para ordenar la columna de manera ascendente",
                    sortDescending: ": Activar para ordenar la columna de manera descendente"
                }
            }
    	});
    	
    	});</script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>
