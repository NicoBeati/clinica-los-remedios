<%@ page import="java.util.Calendar" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
 <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<title>Generar Turnos Mensuales</title>
<style type="text/css">	
        .tablaGrilla {
            width: 100%;
        }
        .columnaLateral_tablaGrilla {
            width: 10%;
        }
        .columnaCentral_tablaGrilla {
            width: 30%;
        }
</style>
</head>
<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  
	
    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesión </a> 
 </div>



	<%
	Calendar calendar = Calendar.getInstance();
       int añoActual = calendar.get(Calendar.YEAR);
	%>
	
	
	

<a href="servletCreaModEspecialista?Param=true" role="button" class="btn btn-primary ms-5 mt-3 mb-2"> Volver a Crear/Modificar Medico </a>

<div class="w-75 px-5  p-5 rounded-1  bg-light  mx-auto" >
	<form action="ServletsPedirTurno" method="get" onsubmit="return confirmacion()">
		
		
	<p class="fs-4 mx-auto mb-3 text-center">Generar turnos</p>	
		
		
		<div class="w-100 d-flex flex-column justify-content-around align-items-center mb-3 ">
		
		
		<label class="form-label m-0 my-2">Ingrese el DNI del Especialista:</label>
		<input type= "text" name="txtDni" class= "form-control w-25" required>
			
		<label class="form-label m-0 my-2">Seleccione el mes:</label>
			
				<select name="ddlMes" class="form-select w-25" >
			     	<option value="0">Enero</option>
			     	<option value="1">Febrero</option>
			     	<option value="2">Marzo</option>
			     	<option value="3">Abril</option>
			     	<option value="4">Mayo</option>
			     	<option value="5">Junio</option>
			     	<option value="6">Julio</option>
			     	<option value="7">Agosto</option>
			     	<option value="8">Septiembre</option>
			     	<option value="9">Octubre</option> 
			     	<option value="10">Noviembre</option>
			     	<option value="11">Diciembre</option> 
			    </select>

			
			<label class="form-label m-0 my-2">Año:</label>
			
			
		
			<input type= "text" name="txtAnio" value="2023" class= "form-control w-25" >
		
		
			<input type="submit" value="Generar Turnos"  class= "btn btn-success mt-4" name="btnGenerarTurnos">
			
			</div>
			

		<%
			boolean agregado = false;
			Object agregoAttr = request.getAttribute("Agrego");
			if (agregoAttr != null) {
			    agregado = (boolean) agregoAttr;
			}
		%>
		<%
		if(request.getParameter("btnGenerarTurnos")!=null){
			if (agregado) {

				 %>
				    <script>
						Swal.fire({
							  
							  icon: 'success',
							  title: 'Horario agregado con éxito',
							  showConfirmButton: false,
							  timer: 1500
							})		 
						</script>
				    <% 
	        
	        }
	        else
	        {
	        	%>
			    <script>
					Swal.fire({
						  
						  icon: 'error',
						  title: 'Error al agregar, llene todos los campos, o ese DNI no existe',
						  showConfirmButton: false,
						  timer: 1500
						})		 
					</script>
			    <%
	        

	        }
		}
		%>

	</form>
	</div>

	
<script>
function confirmacion() {
    return confirm("¿Está seguro que desea realizar esta acción?");
}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</script>
</body>
</html>