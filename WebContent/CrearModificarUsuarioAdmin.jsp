<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Crear/Modificar Usuario Admin</title>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
<style type="text/css">	
        .tablaGrilla {
            width: 100%;
        }
        .columnaLateral_tablaGrilla {
            width: 25%;
        }
        .columnaCentral_tablaGrilla {
            width: 25%;
        }
</style>

<script>
    function validarEmail(email) {
	  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	  if (!re.test(email)) {
	    alert("Por favor, ingrese un correo electrónico válido");
	    return false;
	  }
	  return true;
	}
  </script>
</head>
<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  
	
    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesión </a> 
 </div>

    




<a href="HomeAdmin.jsp" role="button" class="btn btn-primary ms-5 mt-3 mb-2"> Volver a Home </a>

<div class="w-75 px-5  p-5 rounded-1  bg-light  mx-auto" >
	<form action="servletUsuarioAdmin" method="get" id="formAdmin" class="w-100 d-flex flex-column justify-content-between align-items-center " >
	
	<p class="fs-4 mx-auto mb-3">Modificar o Eliminar Admin</p>
	<div class= "d-flex w-100 justify-content-center align-items-center">
		<label class="form-label">Ingrese su Dni :</label>
		<input type= "text" name="txtDniUsuario" class="w-25 form-control mx-5 d-block" minlength="8" maxlength="8">
		<input type="submit" value="Buscar" style="width: 150px" class="btn btn-primary  " name="btnBuscarAdm">
	</div>
	<% if(request.getAttribute("MensajeErrorBuscar")!=null){ %>
	<div class="alert alert-danger my-3 text-center" role="alert">
     				<%=request.getAttribute("MensajeErrorBuscar")%>
    		</div>
   <%} %>
   </form>
  <form action="servletUsuarioAdmin" method="get" id="formAdmin" class="w-100 d-flex flex-column justify-content-between align-items-center" onsubmit="return confirmacion();">
	<p class="fs-5 mx-auto " style="margin-top: 30px" >Ingrese/Modifique los Datos del Admin</p>

		<label class="form-label m-0 my-2">Dni :</label>
		 <input type="text"  class="w-25 form-control mx-5 d-block text-center" required name="txtDni" value="<%= (request.getParameter("btnBuscarAdm") != null && request.getAttribute("dni")!=null) ? request.getAttribute("dni") : "" %>" minlength="8" maxlength="8">
		 <label class="form-label m-0 my-2" >Nombre de usuario :</label>
		<input type="text" name="txtNombre" class="w-25 form-control mx-5 d-block text-center" value="<%= (request.getParameter("btnBuscarAdm") != null && request.getAttribute("nombre")!=null) ? request.getAttribute("nombre") : "" %>">
		
		
	
		
			<label class="form-label m-0 my-2">Email :</label>
			<input type="email" name="txtEmail" required class="w-25 form-control mx-5 d-block text-center"   value="<%= (request.getParameter("btnBuscarAdm") != null && request.getAttribute("email")!=null) ? request.getAttribute("email") : "" %>" >
			<label class="form-label m-0 my-2">Contraseña :</label>
			 <input type="password" required name="txtContrasenia" class="w-25 form-control mx-5 d-block text-center"  value="<%= (request.getParameter("btnBuscarAdm") != null && request.getAttribute("contrasenia")!=null) ? request.getAttribute("contrasenia") : "" %>">
			 <label class="form-label m-0 my-2">Confirmar contraseña :</label>
			 <input type="password" required name="txtConfContrasenia" class="w-25 form-control mx-5 d-block text-center"  value="<%= (request.getParameter("btnBuscarAdm") != null && request.getAttribute("contrasenia")!=null) ? request.getAttribute("contrasenia") : "" %>">


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- Resultados de creación de usuario -->
		<%
	String Mensaje = "" ;
		

 
    if(request.getAttribute("MensajeExito")!=null){
    	Mensaje=request.getAttribute("MensajeExito").toString();
    
    %>
    <script>
		Swal.fire({
			  
			  icon: 'success',
			  title: '<%=Mensaje%>',
			  showConfirmButton: false,
			  timer: 1500
			})		 
		</script>
    <% }
     if(request.getAttribute("MensajeError")!=null){
    	 Mensaje = request.getAttribute("MensajeError").toString();
    	 %>
    	  <script>
		Swal.fire({
			  
			  icon: 'error',
			  title: '<%=Mensaje%>',
			  showConfirmButton: false,
			  timer: 1500
			})		 
		</script>
		
    	 
<%
     }
       
%>

		
		<div class= "d-flex w-100 justify-content-center align-items-center mt-4">
		<input type="submit" value="Crear usuario"  role="button" class="btn btn-success formButtons" onclick="confirmacion()"  style="width: 150px"   name="btnGuardarAdm" >

		<input type="submit" value="Modificar"  role="button" class="btn btn-warning mx-5 formButtons" onclick="confirmacion()"  style="width: 150px"    name="btnModificarAdm" >
		<input type="submit" value="Eliminar"  role="button" class="btn btn-danger formButtons" onclick="confirmacion()"  style="width: 150px"   name="btnEliminarAdm" >
		</div>
		</form>
		</div>
		



<script>
function validarContraseñas() {
    var contrasenia = document.getElementById("txtContrasenia").value;
    var confContrasenia = document.getElementById("txtConfContrasenia").value;
    
    if (contrasenia !== confContrasenia) {
        alert("Las contraseñas no coinciden. Por favor, verifique.");
        return false; // Detiene el envío del formulario
    }
    
    return true; // Permite el envío del formulario
}
</script>




<script>
function confirmacion() {
	alert("Esta seguro de que desea realizar esta acción ? ")
	
	
}
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>