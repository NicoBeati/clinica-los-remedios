<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="entidad.Usuarios" %>
<%@ page import="negocioImpl.UsuariosNegocioImpl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link href="
https://cdn.jsdelivr.net/npm/sweetalert2@11.7.12/dist/sweetalert2.min.css
" rel="stylesheet">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
<link rel="stylesheet" href="@sweetalert2/theme-borderless/borderless.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
<style type="text/css">	
		
        .tablaGrilla {
            width: 100%;
        }
        .columnaLateral_tablaGrilla {
            width: 30%;
        }
        .columnaCentral_tablaGrilla {
            width: 20%;
        }
</style>

</head>
<body style="background: #9FC9D3; font-family: 'Montserrat', sans-serif;">

<form method="get" action="ServletLogin" >

<div style="width: 500px" class="p-5 rounded-1 bg-light d-flex flex-column justify-content-center position-absolute top-50 start-50 translate-middle">
<img  class="w-100 d-block mx-auto mb-5" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Logo-Largo" border="0">
<p class="fs-3 text-center mb-3 d-block" >Inicio de sesión</p>
<input type= "text" name="txtUsuario" class="form-control mb-3 text-center " placeholder="Ingrese su usuario">

<input type= "password" name="txtPwd" class="form-control mb-3 text-center" placeholder="Ingrese su contraseña" >

<input type="submit" value="Ingresar" name="btnIngresar" class="btn btn-primary btn-block my-4 text-center">

<a href="ResetPwd.jsp" class="mx-auto"> ¿Olvidó su contraseña? </a> 

<% 	String Mensaje = ""; 
		if(request.getAttribute("MensajeServeletLogin")!=null){
		Mensaje= request.getAttribute("MensajeServeletLogin").toString();
		%>
		<div class="alert alert-danger my-3 text-center" role="alert">
 		<%=Mensaje %>
		</div>
		<%
		}
		%>
		
		
		
		





</div>

</form>

<script src="
https://cdn.jsdelivr.net/npm/sweetalert2@11.7.12/dist/sweetalert2.all.min.js
"></script>
<script src="sweetalert2/dist/sweetalert2.min.js"></script>
<!--  <%
if (request.getAttribute("MensajeServeletLogin") != null) {
%>
<script>

    Swal.fire({
        icon: 'error',
        title: 'Ups...',
        text: '<%= request.getAttribute("MensajeServeletLogin") %>',
        	backdrop: `
	            rgba(0,0,123,0.4)
	            url("https://sweetalert2.github.io/images/nyan-cat.gif")
	      
	            left top
	            no-repeat
          ` 
      

        //footer: '<a href="">Why do I have this issue?</a>'
    })
</script>
<%
}
%>-->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
 



</body>
</html>