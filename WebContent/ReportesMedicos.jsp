<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="entidad.Especialidad"%>
<%@ page import="entidad.ReporteMedico"%>
<%@ page import="java.util.ArrayList"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reportes Médico</title>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    
    
</head>
<style type="text/css">	
        .tablaGrilla {
            width: 100%;
        }
        .columnaLateral_tablaGrilla {
            width: 20%;
        }
        .columnaCentral_tablaGrilla {
            width: 30%;
        }
        .usuario {
		    position: absolute;
		    top: 0;
		    right: 10px;
		    margin-top: 10px;
		    margin-right: 10px;
		    text-align: right;
  		}
</style>
<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  
	
    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesi�n </a> 
 </div>
 
  <a href="HomeAdmin.jsp" role="button" class="btn btn-primary ms-5 mt-3 mb-2"> Volver a Home </a> 
  
  <div class=" px-5  p-5 rounded-1  bg-light  mx-auto" style="width: 90%;" >
<p class="fs-3 mx-auto mb-5 text-center">Reporte médicos</p>
	<form action="servletReportesMedico" method="get">

		<div class="w-100 d-flex justify-content-around align-items-center mb-4 ">

						<label for="especialidad">Especialidad:</label>
					   	<select name="Especialidades" class="form-select w-25" onchange="cargarEspecialidades()">
					    	<% List<Especialidad> listaEsp = (List<Especialidad>) request.getAttribute("listaEsp");
									if (listaEsp != null) {
					    			for (Especialidad esp : listaEsp) { %>
					        			<option value="<%=esp.getCodEspecialidad().toString()%>"><%=esp.getDescripcionEspecialidad().toString()%></option>
					    			<% }
					  		} %>
					    </select>
					
					
						<label for="mes">Mes:</label>
					    <select id="mes" name="mes" class= "form-select w-25" onchange="actualizarTurnos()">
					      <option value="01">Enero</option>
					      <option value="02">Febrero</option>
					      <option value="03">Marzo</option>
					      <option value="04">Abril</option>
					      <option value="05">Mayo</option>
					      <option value="06">Junio</option>
					      <option value="07">Julio</option>
					      <option value="08">Agosto</option>
					      <option value="09">Septiembre</option>
					      <option value="10">Octubre</option>
					      <option value="11">Noviembre</option>
					      <option value="12">Diciembre</option>
					    </select>
				
				<input type="submit" value="Filtrar" class="btn btn-success" name="btnFiltrar">
				
				
		</div>
		
		<div>
			<% 
			ArrayList<ReporteMedico> listaRepMed = null;
			
			if (request.getAttribute("listaRepMed") != null) {
	   			
	   			listaRepMed = (ArrayList<ReporteMedico>)request.getAttribute("listaRepMed"); 
			}
			 %>
			<table id="tablareporteMed" class="display table" >
			    <thead class="mt-5  table-info" style="width: 100% !important;">
			      <tr>
			        <th>DNI</th>
			        <th>Nombre</th>
			        <th>Apellido</th>
			        <th>Especialidad</th>
			        <th>Cantidad de Turnos</th>
			      </tr>
			    </thead>
				    <tbody>
				    <% 
				    	if (listaRepMed != null){
				    		for(ReporteMedico rm : listaRepMed){ %>
				    			<tr>
					    			<td><%= rm.getDni_Especialista_Turnos() %> </td>
					    			<td> <%= rm.getNombre_Especialistas() %> </td>
					    			<td> <%= rm.getApellido_Especialistas() %> </td>
					    			<td> <%= rm.getCod_Especialidad_Turnos() %></td>
					    			<td> <%= rm.getContados() %> </td>
					    		</tr>
				    <%
				    	}
				    }
				    %>
				   
				      
				    </tbody>
		  </table>
		</div>
	</form>
	</div>
	 <script type="text/javascript">
    $(document).ready(function(){
    	$('#tablareporteMed').DataTable({
    		 language: {
                 processing: "Procesando...",
                 lengthMenu: "Mostrar _MENU_ registros",
                 zeroRecords: "No se encontraron resultados",
                 emptyTable: "Ningún dato disponible en esta tabla",
                 info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                 infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                 infoFiltered: "(filtrado de un total de _MAX_ registros)",
                 infoPostFix: "",
                 search: "Buscar:",
                 thousands: ",",
                 loadingRecords: "Cargando...",
                 paginate: {
                     first: "Primero",
                     last: "�ltimo",
                     next: "Siguiente",
                     previous: "Anterior"
                 },
                 aria: {
                     sortAscending: ": Activar para ordenar la columna de manera ascendente",
                     sortDescending: ": Activar para ordenar la columna de manera descendente"
                 }
             }
    	});
    	});</script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>