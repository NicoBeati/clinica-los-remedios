<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="entidad.Turnos" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
     <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
    <title>Tabla de Turnos</title>
  
</head>
<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  
	
    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesión </a> 
 </div>
 
 <a href="VistaMedico.jsp" role="button" class="btn btn-primary ms-5 mt-3 mb-2"> Volver a Home </a> 

    <div class=" px-5  p-5 rounded-1  bg-light  mx-auto" style="width: 90%;" >
    <form action="ServletHistoriaClinica" method="get">
    
	<p class="fs-3 mx-auto mb-5 text-center ">Historia clínica</p>
	<div class="d-flex  justify-content-center align-items-center">
	    <input type="text" class="form-control w-25" name="dni" placeholder="Ingrese DNI">
	    <input type="submit" class= "btn btn-primary" value="Buscar">
	    </div>
	</form>
	<div>
    <table class="display table" id="TablaTurnos" >
    <thead class="mt-5  table-info" style="width: 100% !important;">
        <tr>
            <th>Código de turno</th>
            <th>DNI del Paciente</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th>DNI del médico</th>
            <th>Asistencia</th>
            <th>Motivo consulta</th>
        </tr>
       </thead>
       <tbody>
        <%
            List<Turnos> turnosList = (List<Turnos>) request.getAttribute("turnosPList");
            if (turnosList != null) {
                for (Turnos turnos : turnosList) {
                    String motivoConsulta = turnos.getMotivo_Consulta_Turnos();
                    if (motivoConsulta == null) {
                        motivoConsulta = "";
                    }
        %>
        <tr>
            <td><%= turnos.getCod_Turnos() %></td>
            <td><%= turnos.getDni_Paciente_Turnos() %></td>
            <td><%= turnos.getFecha_Turnos() %></td>
            <td><%= turnos.getHorario_Turnos() %></td>
            <td><%= turnos.getDni_Especialista_Turnos() %></td>
            <td><input type="text" name="txtAsistencia" value="<%= turnos.getAsistencia_Turnos() %>" disabled></td>
            <td><input type="text" name="txtMotivo" value="<%= motivoConsulta %>" disabled></td>
        </tr>
        <%
                }
            }
        %>
        </tbody>
    </table>
    
    </div>
   </div>
    <script type="text/javascript">
    $(document).ready(function(){
    	$('#TablaTurnos').DataTable({
    		 language: {
                 processing: "Procesando...",
                 lengthMenu: "Mostrar _MENU_ registros",
                 zeroRecords: "No se encontraron resultados",
                 emptyTable: "Ningún dato disponible en esta tabla",
                 info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                 infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                 infoFiltered: "(filtrado de un total de _MAX_ registros)",
                 infoPostFix: "",
                 search: "Buscar:",
                 thousands: ",",
                 loadingRecords: "Cargando...",
                 paginate: {
                     first: "Primero",
                     last: "�ltimo",
                     next: "Siguiente",
                     previous: "Anterior"
                 },
                 aria: {
                     sortAscending: ": Activar para ordenar la columna de manera ascendente",
                     sortDescending: ": Activar para ordenar la columna de manera descendente"
                 }
             }
    	});
    	});</script>
    	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
    </body>
</html>