<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home Administrador</title>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
<style type="text/css">	
        .tablaGrilla {
            width: 100%;
        }
        .columnaLateral_tablaGrilla {
            width: 20%;
        }
        .columnaCentral_tablaGrilla {
            width: 30%;
        }
</style>
</head>
<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  

    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">¡Bienvenido <%= session.getAttribute("UsuarioAdmin") %>!</span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesión </a> 

    

</div>
<div class="d-flex position-absolute top-50 start-50 translate-middle flex-column align-items-center w-75 ">

	
			<a href="CrearModificarUsuarioAdmin.jsp" role="button" class="btn btn-light mb-2 w-25 d-flex justify-content-between align-items-center" ><img width="60px" src="Imagenes/administrador.png">Crear/Modificar Usuario Administrador </a> 

			<a href="servletCreaModEspecialista?Param=true" role="button" class="btn btn-light mb-2 w-25 d-flex justify-content-between align-items-center"><img width="60px" src="Imagenes/medico.png"> Crear/Modificar Usuario Médico </a>
			
		<a href="ServletsInsertarPacientes?Param=true" role="button" class="btn btn-light mb-2 w-25 d-flex justify-content-between align-items-center"><img width="60px" src="Imagenes/pacientes.png"> Registrar/Modificar Paciente </a> 
		
			<a href="servletUsuarioAdmin?Param=true" role="button" class="btn btn-light mb-2 w-25 d-flex justify-content-start  align-items-center"><img style="display:block;margin-right: 70px;" width="60px" src="Imagenes/listar.png"> Listar Usuarios </a>
		
			<a href="ServletListarPacientes?Param=true" role="button" class="btn btn-light mb-2 w-25 d-flex justify-content-start align-items-center"><img  style="display:block;margin-right: 70px;" width="60px" src="Imagenes/listar.png"> Listar Pacientes </a>
		
			<a href="ServletsPedirTurno?Param=true" role="button" class="btn btn-light mb-2 w-25 d-flex justify-content-start align-items-center"  mb-2 w-25"><img  style="display:block;margin-right: 70px;" width="60px" src="Imagenes/turnos2.png"> Pedir Turnos </a> 
		
			<a href="ServletEliminarTurnos?Param=true" role="button" class="btn btn-light mb-2 w-25 d-flex justify-content-start align-items-center"><img  style="display:block;margin-right: 70px;"  width="60px" src="Imagenes/eliminarTurno.png"> Eliminar Turnos </a> 
			
			<a href="servletReportesMedico?Param=true" role="button" class="btn btn-light mb-2 w-25 d-flex justify-content-start align-items-center"><img style="display:block;margin-right: 70px;" width="60px" src="Imagenes/reportes.png"> Reportes Médicos </a>
		
			<a href="ReportesPacientes.jsp" role="button" class="btn btn-light mb-2 w-25 d-flex justify-content-start align-items-center"><img width="60px"  style="display:block;margin-right: 70px;" src="Imagenes/reportes.png"> Reportes Pacientes </a> 
</div>



			
	
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>