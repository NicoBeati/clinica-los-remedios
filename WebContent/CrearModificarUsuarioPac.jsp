<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="entidad.Provincia"%>
<%@ page import="entidad.Localidad"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <script src='jquery-ui.min.js' type='text/javascript'></script>
 <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
 <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
 <script src='datepicker-es.js' type='text/javascript'></script><link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    function validarEmail(email) {
      var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (!re.test(email)) {
        alert("Por favor, ingrese un correo electrónico válido");
        return false;
      }
      return true;
    }
  </script>

</head>

<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  
	
    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesi�n </a> 
 </div>
 
 
<a href="HomeAdmin.jsp" role="button" class="btn btn-primary ms-5 mt-3 mb-2"> Volver a Home </a>
	<% /* PARA AUTO-POPULAR SEXO*/
    Boolean esMasculino = false;
    Boolean esFemenino = false;
    if (request.getAttribute("sexo") != null) {
        esMasculino = request.getAttribute("sexo").equals('M');
        esFemenino = request.getAttribute("sexo").equals('F');
    } 
	%>
	
	<%
	String prov1="";
	if (request.getAttribute("provincia") != null) prov1= (String)request.getAttribute("provincia");
	%>
	
	<%
	String loc1="";
	if (request.getAttribute("localidad") != null) loc1= (String)request.getAttribute("localidad");
	%>
	<div class="w-75 px-5  p-5 rounded-1  bg-light  mx-auto" >
<form action="ServletsInsertarPacientes" method="get" class="w-100 d-flex flex-column justify-content-between align-items-center" >
	<p class="fs-4 mx-auto mb-3 " >Modificar o Eliminar Paciente</p>
		
		<div class="w-100 d-flex justify-content-around align-items-center mb-4 mt-2">
			<label class="form-label">Ingrese  DNI:</label>
	

			<input type= "text" name="txtDniUsuario" minlength="8" maxlength="8" class="form-control w-25">
			<input type="submit" value="Buscar" class="btn btn-primary   " style="width: 150px !important" name="btnBuscarPac">
			</div>
	
	<p class="fs-4 mx-auto mb-3 text-center w-100" >Ingrese/Modifique los Datos del Paciente</p>
			
		
				<label class="form-label  m-0 my-2"> DNI:</label>
			
			<input type="text" name="txtDni" class="w-25 form-control mx-5 d-block text-center" value="<%= (request.getParameter("btnBuscarPac") != null) ? request.getAttribute("dni") : "" %>" minlength="8" maxlength="8">
		
			<label class="form-label  m-0 my-2"> Nombre:</label>
			<input type="text" name="txtNombre" class="w-25 form-control mx-5 d-block text-center"  value="<%= (request.getParameter("btnBuscarPac") != null) ? request.getAttribute("nombre") : "" %>">
			
		<label class="form-label  m-0 my-2"> Apellido:</label>
			<input type="text" name="txtApellido"  class="w-25 form-control mx-5 d-block text-center" value="<%= (request.getParameter("btnBuscarPac") != null) ? request.getAttribute("apellido") : "" %>"></td>
			
	
			<label class="form-label  m-0 my-2"> Sexo:</label>
			<div class="d-flex justify-content-around align-items-center">
				<div class="d-flex justify-content-center me-3"><input type="radio" class="form-check-input " id="FM" name="FemeninoMasculino" value='F' <%= esFemenino ? "checked" : "" %>><label for="Femenino">Femenino</label></div>
				<div class="d-flex justify-content-center"><input type="radio" id="FM" class="form-check-input " name="FemeninoMasculino" value='M' <%= esMasculino ? "checked" : "" %>><label for="Masculino">Masculino</label></div>
			</div>
			
				<label class="form-label  m-0 my-2"> Nacionalidad:</label>
			
			
			<input type="text"   class="w-25 form-control mx-5 d-block text-center" name="txtNacionalidad" value="<%= (request.getParameter("btnBuscarPac") != null) ? request.getAttribute("nacionalidad") : "" %>">
			
				<label class="form-label  m-0 my-2"> Fecha de Nacimiento:</label>
		
			<input type="date" class="w-25 form-control mx-5 d-block text-center" id="start" name="trip-start" value="<%= (request.getParameter("btnBuscarPac") != null) ? request.getAttribute("fecha_nac") : "" %>" min="1900-01-01" max="2023-06-29">
			
			<label class="form-label  m-0 my-2"> Provincia:</label>
		
				<select id="Provincias" name="Provincias" class="form-select w-25 text-center">

				    <% List<Provincia> listaProv = (List<Provincia>) request.getAttribute("listaProv");
		   				if (listaProv != null) {
		       			for (Provincia prov : listaProv) { %>
		           			<option <%=prov1.equals(prov.getNombreProvincia().toString()) ? "selected" : "" %> value="<%=prov.getCodProvincia().toString()%>"><%=prov.getNombreProvincia().toString()%></option>
		       			<% }
			   		} %>
				</select>
			
		<label class="form-label  m-0 my-2"> Localidad :</label>
			
			 	<select id="Localidades" name="Localidades" class="form-select w-25 text-center">
				      <% List<Localidad> listaLoc = (List<Localidad>) request.getAttribute("listaLoc");
		    				if (listaLoc != null) {
		        			for (Localidad loc : listaLoc) { %>
		            			<option <%=loc1.equals(loc.getNombreLocalidad().toString()) ? "selected" : "" %> value="<%=loc.getCodLocalidad().toString()%>" class="<%=loc.getCodProvincia()%>"><%=loc.getNombreLocalidad()%></option>
		        			<% }
		    		} %>
				</select>

		<label class="form-label  m-0 my-2"> Direcci�n :</label>
		
			
			<input type= "text" name="txtDireccion" class="w-25 form-control mx-5 d-block text-center" value="<%= (request.getParameter("btnBuscarPac") != null) ? request.getAttribute("direccion") : "" %>"></td>
			
			<label class="form-label  m-0 my-2"> Correo Electr�nico :</label>
			
			<input type= "email" name="txtEmail" class="w-25 form-control mx-5 d-block text-center"  value="<%= (request.getParameter("btnBuscarPac") != null) ? request.getAttribute("email") : "" %>">
			
			<label class="form-label  m-0 my-2"> Tel�fono :</label>
			
			
			<input type= "text" name="txtTelefono" class="w-25 form-control mx-5 d-block text-center" value="<%= (request.getParameter("btnBuscarPac") != null) ? request.getAttribute("telefono") : "" %>">
			
			<div class= "d-flex w-100 justify-content-around align-items-center mt-4">
			<input type="submit"  role="button" class="btn btn-success formButtons" onclick="confirmacion()"  style="width: 150px" value="Guardar" name="btnGuardarPac">
			<input type="submit"  role="button" class="btn btn-warning formButtons" onclick="confirmacion()"  style="width: 150px" value="Modificar" name="btnModificarPac">
			<input type="submit" role="button" class="btn btn-danger formButtons" onclick="confirmacion()"  style="width: 150px"  value="Eliminar" name="btnEliminarPac">
			</div>
		

</form>
</div>
<%
	boolean agregado = false;
	Object agregoAttr = request.getAttribute("Agrego");
	if (agregoAttr != null) {
	    agregado = (boolean) agregoAttr;
	}
%>

<%
	if (request.getParameter("btnGuardarPac") != null) {
        if (agregado) {
        	%>
        	
        	<script>
		Swal.fire({
			  
			  icon: 'success',
			  title: 'Paciente creado con �xito.',
			  showConfirmButton: false,
			  timer: 1500
			})		 
		</script>
        	<% 
        }
        else
        {
%>
        	
        	<script>
		Swal.fire({
			  
			  icon: 'error',
			  title: 'Error al agregar, llene todos los campos, o ese DNI ya existe',
			  showConfirmButton: false,
			  timer: 2000
			})		 
		</script>
        	<% 
        	
        }
    }
%>

<%
boolean borrado = false;
Object borroAttr = request.getAttribute("Borro");
if (borroAttr != null) {
    borrado = (boolean) borroAttr;
}
%>

<%
if (request.getParameter("btnEliminarPac") != null) {
        if (borrado) {
%>
        	
        	<script>
		Swal.fire({
			  
			  icon: 'success',
			  title: 'Paciente Eliminado con �xito',
			  showConfirmButton: false,
			  timer: 1500
			})		 
		</script>
        	<% 
        	
        }
        else
        {
%>
        	
        	<script>
		Swal.fire({
			  
			  icon: 'error',
			  title: 'Error al eliminar, ese DNI no �xiste.',
			  showConfirmButton: false,
			  timer: 2000
			})		 
		</script>
        	<% 
        	
        	
        }
    }
%>

<%
boolean modificado = false;
Object modificadoAttr = request.getAttribute("Modifico");
if (modificadoAttr != null) {
    modificado = (boolean) modificadoAttr;
}
%>

<%
if (request.getParameter("btnModificarPac") != null) {
    if (modificado) {
    	%>
    	<script>
		Swal.fire({
			  
			  icon: 'success',
			  title: 'Paciente modificado con �xito.',
			  showConfirmButton: false,
			  timer: 1500
			})		 
		</script>
		<%
		 
        }
        else
        {
        	%>
        	<script>
    		Swal.fire({
    			  
    			  icon: 'error',
    			  title: 'Error al modificar, llene todos los campos, o ese DNI no existe.',
    			  showConfirmButton: false,
    			  timer: 1500
    			})		 
    		</script>
    		<%
        	
        }
}
%>

<script>
		$('#Provincias').change(function(){
    		$('#Localidades option')
        		.hide() // hide all
        		.filter('[class="'+$(this).val()+'"]') // filter options with required value
            	.show()[0].selected=true; // and show them
		});

		$(document).ready(function(){
			$('#Localidades option')
        		.hide()
        		.filter('[class="'+$('#Provincias').find(":selected").val()+'"]')
            	.show();
    	}); 
    	
		function confirmacion() {
		      return confirm("¿Está seguro que desea realizar esta acción?");
		}
</script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
</body>
</html>