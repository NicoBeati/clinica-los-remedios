<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List" %>
<%@ page import="entidad.Especialidad"%>
<%@ page import="entidad.Provincia"%>
<%@ page import="entidad.Localidad"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
	<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		 <script src='jquery-ui.min.js' type='text/javascript'></script>
		 <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
		 <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
		 <script src='datepicker-es.js' type='text/javascript'></script>
		 <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
		 
		 <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
		<title>Insert title here</title>
		<style type="text/css">	
		        .tablaGrilla {
		            width: 100%;
		        }
		        .columnaLateral_tablaGrilla {
		            width: 10%;
		        }
		        .columnaCentral_tablaGrilla {
		            width: 30%;
		        }
		</style>
	</head>
	
	<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
	
		
		<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
		 
		    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
		   
		    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
		 
		    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesión </a> 
		
		</div>
		
		<div class="w-100 px- d-flex justify-content-between align-items-center " >
<a href="HomeAdmin.jsp" role="button" class="btn btn-primary ms-5 mt-3 mb-4"> Volver a Home </a>

<a href="AgregarHorarioEspecialista.jsp" role="button" class="btn btn-warning me-5 mt-3 mb-2">Ir a Agregar horarios de especialista </a>
 </div>
		
		
		<% /* PARA AUTO-POPULAR SEXO*/
		    Boolean esMasculino = false;
		    Boolean esFemenino = false;
		    if (request.getAttribute("sexo") != null) {
		        esMasculino = request.getAttribute("sexo").equals('M');
		        esFemenino = request.getAttribute("sexo").equals('F');
		    } 
		%>
	
		<%
			String espe1="";
			if (request.getAttribute("especialidad") != null) espe1= (String)request.getAttribute("especialidad");
		%>
		
		<%
			String prov1="";
			if (request.getAttribute("provincia") != null) prov1= (String)request.getAttribute("provincia");
		%>
		
		<%
			String loc1="";
			if (request.getAttribute("localidad") != null) loc1= (String)request.getAttribute("localidad");
		%>
		
	
		<div class="w-75 px-5 mx-auto p-5 rounded-1  bg-light d-flex flex-column justify-content-center align-items-center " >
		
		<form action="servletCreaModEspecialista" method="get" >
		
			<p class="fs-4 mx-auto mb-3 text-center ">Modificar o Eliminar Médico</p>
				<div class= "d-flex w-100 justify-content-around align-items-center">
					<label class="form-label">Ingrese su Dni :</label>
					<input type= "text" name="txtUsuario" class="w-25 form-control mx-5 d-block" minlength="8" maxlength="8">
					<input type="submit" value="Buscar" style="width: 150px" class="btn btn-primary  " name="btnBuscarMed">
				</div>
			<% if(request.getAttribute("MensajeErrorBuscar")!=null){ %>
				<div class="alert alert-danger my-3 text-center" role="alert">
			     	<%=request.getAttribute("MensajeErrorBuscar").toString()%>
			    </div>
		    
		   <%} %>
	   </form>
	   <form action="servletCreaModEspecialista" method="get" onsubmit="return confirmacion() && validarContrasenias();" class="w-100 d-flex flex-column justify-content-between align-items-center ">
			<p class="fs-5 mx-auto text-center " style="margin-top: 30px" >Ingrese/Modifique los Datos del Admin</p>
	
			<label class="form-label m-0 my-2">Dni :</label>
			<input type="text" required class="w-25 form-control mx-5 d-block text-center" name="txtDni" value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("dni")!=null) ? request.getAttribute("dni") : "" %>" minlength="8" maxlength="8">
			<label class="form-label m-0 my-2" >Nombre:</label>
			<input type="text" required name="txtNombre" class="w-25 form-control mx-5 d-block text-center" value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("nombre")!=null) ? request.getAttribute("nombre") : "" %>">
			
			<label class="form-label m-0 my-2" >Apellido:</label>
			<input type="text" required name="txtApellido" class="w-25 form-control mx-5 d-block text-center" value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("apellido")!=null) ? request.getAttribute("apellido") : "" %>">
			
			
			<label class="form-label  m-0 my-2"> Sexo:</label>
			<div class="d-flex justify-content-around align-items-center">
				<div class="d-flex justify-content-center me-3"><input type="radio" name="rdBtnSexo" value="F" <%= esFemenino ? "checked" : "" %>><label for="Femenino">Femenino</label></div>
				<div class="d-flex justify-content-center"><input type="radio" name="rdBtnSexo" value="M" <%= esMasculino ? "checked" : "" %>><label for="Masculino">Masculino</label></div>
			</div>
			
			
			
	
			
			
			<label class="form-label m-0 my-2" >Nacionalidad:</label>
			<input type="text" required name="txtNacionalidad" class="w-25 form-control mx-5 d-block text-center" value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("nacionalidad")!=null) ? request.getAttribute("nacionalidad") : "" %>">
			
			<label class="form-label m-0 my-2" >Fecha de nacimiento:</label>
			<input type="date" required id="start" name="dateFechaNac" class="form-control w-25 text-center" value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("fecha_nac")!=null) ? request.getAttribute("fecha_nac") : "" %>"
				min="1900-01-01" max="2023-06-29">
					 
			<label class="form-label m-0 my-2" >Provincia:</label>
			<select id="Provincias" name="Provincias" class="form-select w-25 text-center">
					      <% List<Provincia> listaProv = (List<Provincia>) request.getAttribute("listaProv");
			    				if (listaProv != null) {
			        			for (Provincia prov : listaProv) { %>
			            			<option <%=prov1.equals(prov.getNombreProvincia().toString()) ? "selected" : "" %> value="<%=prov.getCodProvincia().toString()%>"><%=prov.getNombreProvincia().toString()%></option>
			        			<% }
			    		} %>
					    </select>
			<label class="form-label m-0 my-2" >Localidad:</label>
			<select id="Localidades" name="Localidades" class="form-select w-25 text-center">
					      <% List<Localidad> listaLoc = (List<Localidad>) request.getAttribute("listaLoc");
			    				if (listaLoc != null) {
			        			for (Localidad loc : listaLoc) { %>
			            			<option <%=loc1.equals(loc.getNombreLocalidad().toString()) ? "selected" : ""%> value="<%=loc.getCodLocalidad().toString()%>" class="<%=loc.getCodProvincia()%>"><%=loc.getNombreLocalidad().toString()%></option>
			        			<% }
			    		} %>
			</select>
					    
			<label class="form-label m-0 my-2" >Dirreción:</label>	    
			<input type="text" required class="w-25 form-control mx-5 d-block text-center"  name="txtDireccion" value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("direccion")!=null) ? request.getAttribute("direccion") : "" %>">
				
					 
			<label class="form-label m-0 my-2">Email :</label>
			<input type="email" required name="txtEmail" class="w-25 form-control mx-5 d-block text-center" value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("email")!=null) ? request.getAttribute("email") : "" %>">
			<label class="form-label m-0 my-2">Telefono :</label>
			<input type="text" required class="w-25 form-control mx-5 d-block text-center" name="txtTelefono" value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("telefono")!=null) ? request.getAttribute("telefono") : "" %>">
			<label class="form-label m-0 my-2">Especialidad :</label>
			<select name="Especialidades" class="form-select w-25 text-center">
			      <% List<Especialidad> listaEsp = (List<Especialidad>) request.getAttribute("listaEsp");
	    				if (listaEsp != null) {
	        			for (Especialidad esp : listaEsp) { %>
	            			<option <%=espe1.equals(esp.getCodEspecialidad()) ? "selected" : "" %> value="<%=esp.getCodEspecialidad().toString()%>"><%=esp.getDescripcionEspecialidad().toString()%></option>
	        			<% }
	    		} %>
		    </select>
		
			<label class="form-label m-0 my-2">Contraseña :</label>
				 <input type="password" required name="txtContrasenia" class="w-25 form-control mx-5 d-block text-center"  value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("contrasenia")!=null) ? request.getAttribute("contrasenia") : "" %>">
				 
			 <label class="form-label m-0 my-2">Confirmar contraseña :</label>
			 <input type="password" required name="txtConfContrasenia" class="w-25 form-control mx-5 d-block text-center"  value="<%= (request.getParameter("btnBuscarMed") != null && request.getAttribute("conf_contrasenia")!=null) ? request.getAttribute("conf_contrasenia") : "" %>">
	
	
			<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	
	<!-- Resultados de creación de usuario -->
			<%
		String Mensaje = "" ;
					
	    if(request.getAttribute("MensajeExito")!=null){
	    	Mensaje=request.getAttribute("MensajeExito").toString();
	    
	    %>
	    <script>
			Swal.fire({
				  
				  icon: 'success',
				  title: '<%=Mensaje%>',
				  showConfirmButton: false,
				  timer: 1500
				})		
			</script>
	    <% }
	     if(request.getAttribute("MensajeError")!=null){
	    	 Mensaje = request.getAttribute("MensajeError").toString();
	    	 %>
	    	 <div class="alert alert-danger my-3 text-center" role="alert">
				<%=Mensaje %>
			</div>
	<%
	     }
	       
	%>
				<div class= "d-flex w-100 justify-content-center align-items-center mt-4">
				<input type="submit" value="Crear usuario"  role="button" class="btn btn-success formButtons" style="width: 150px"  onclick="return confirmacion()" name="btnGuardarMed" >
		
				<input type="submit" value="Modificar"  role="button" class="btn btn-warning mx-5 formButtons"  style="width: 150px"   onclick="return confirmacion()" name="btnModificarMed" >
				<input type="submit" value="Eliminar"  role="button" class="btn btn-danger formButtons"   style="width: 150px"  onclick="return confirmacion()" name="btnEliminarMed" >
				</form>
				</div>
			</div>
		
		<script>
			$('#Provincias').change(function(){
	    		$('#Localidades option')
	        		.hide() // hide all
	        		.filter('[class="'+$(this).val()+'"]') // filter options with required value
	            	.show()[0].selected=true; // and show them
			});
			
			$(document).ready(function(){
				$('#Localidades option')
	        		.hide()
	        		.filter('[class="'+$('#Provincias').find(":selected").val()+'"]')
	            	.show();
	    	}); 
	    	 
	    	function validarFormulario() {
			    var email = document.getElementById('txtEmail').value;
			    if (!validarEmail(email)) {
			      return false; // Detiene el envío del formulario
			    }
			    return confirmacion();
			  }
			
			  function validarEmail(email) {
			    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			    if (!re.test(email)) {
			      alert("Por favor, ingrese un correo electrónico válido");
			      return false;
			    }
			    return true;
			  }
			
			  function confirmacion() {
			    return confirm("¿Está seguro que desea realizar esta acción?");
			  }
			  
				function validarContrasenias() {
				    var contrasenia = document.getElementById("txtContrasenia").value;
				    var confContrasenia = document.getElementById("txtConfContrasenia").value;
				    
				    if (contrasenia !== confContrasenia) {
				        alert("Las contraseñas no coinciden. Por favor, verifique.");
				        
				        return false; // Detiene el envío del formulario
				    }
				    
				    return true; // Permite el envío del formulario
				} 	
			
		</script>
	</body>
	</html>