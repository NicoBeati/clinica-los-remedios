<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="entidad.Especialidad" %>
<%@ page import="entidad.ReporteMedico" %>
<%@ page import="entidad.ReportePaciente" %>
<%@ page import="java.util.ArrayList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Reportes de Pacientes</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"> </script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"> </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <style type="text/css">
        .tablaGrilla {
            width: 100%;
        }

        .columnaLateral_tablaGrilla {
            width: 20%;
        }

        .columnaCentral_tablaGrilla {
            width: 30%;
        }

        .usuario {
            position: absolute;
            top: 0;
            right: 10px;
            margin-top: 10px;
            margin-right: 10px;
            text-align: right;
        }
    </style>
</head>
<body  style="background: #9FC9D3; margin:0; font-family: 'Montserrat', sans-serif;">
<div class="w-100 px-5  bg-light d-flex justify-content-between align-items-center " >
 
  
	
    <a style="height: 60px"><img class="h-100" src="https://i.ibb.co/Q8dbNNY/Logo-Largo.png" alt="Bootstrap" ></a>
   
    <span class="fs-4 " style="display: block; margin-left: -40px">Usuario: <%= session.getAttribute("UsuarioAdmin") %> </span>
 
    <a href="Login.jsp" role="button" class="btn btn-danger"> Cerrar Sesi�n </a> 
 </div>
  <a href="HomeAdmin.jsp" role="button" class="btn btn-primary ms-5 mt-3 mb-2"> Volver a Home </a> 
  <div class=" px-5  p-5 rounded-1  bg-light  mx-auto" style="width: 90%;" >
<form action="servletReportesPacientes" method="get">

   <p class="fs-3 mx-auto mb-5 text-center">Reporte pacientes</p>
          
                <div class="w-100 d-flex justify-content-around align-items-center mb-3 ">

                    <label for="dni">DNI:</label>
                    <input type="text" name="txtDni" class= "form-control w-25" value="">
              
               
                    <label for="especialidad">Especialidad:</label>
                    <input type="text" class="form-control w-25" name="txtEspecialidad" value="">
               
                    <input type="submit" value="Reporte paciente" class= "btn btn-success" name="btnFiltrar">
       
				</div>
 
</form>


    <% 
    ArrayList<ReportePaciente> listaRepPac = (ArrayList<ReportePaciente>) request.getAttribute("listaRepPac");
    %>
    <table id="TablareportesPaciente" class="display table">
        <thead  class="mt-5  table-info" style="width: 100% !important;" >
        <tr>
            <th>Mes</th>
            <th>DNI</th>
            <th>Nombre</th>
            <th>Especialidad</th>
            <th>Asistencias</th>
        </tr>
        </thead>
        <tbody>
        <% 
        if (listaRepPac != null) {
            for (ReportePaciente rp : listaRepPac) { 
                if(rp.getDni_Paciente_Turnos()!="00000000"){
        %>
        <tr>
            <td><%= rp.getMes_Paciente_Turnos() %></td>
            <td><%= rp.getDni_Paciente_Turnos() %></td>
            <td><%= rp.getNombre_Paciente_Turnos() %></td>
            <td><%= rp.getNombre_Especialidad_Turnos() %></td>
            <td><%= rp.getContador() %></td>
        </tr>
        <% 
                }
            }
        }
        %>
        </tbody>
    </table>


   </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#TablareportesPaciente').DataTable({
        	 language: {
                 processing: "Procesando...",
                 lengthMenu: "Mostrar _MENU_ registros",
                 zeroRecords: "No se encontraron resultados",
                 emptyTable: "Ning�n dato disponible en esta tabla",
                 info: "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                 infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
                 infoFiltered: "(filtrado de un total de _MAX_ registros)",
                 infoPostFix: "",
                 search: "Buscar:",
                 thousands: ",",
                 loadingRecords: "Cargando...",
                 paginate: {
                     first: "Primero",
                     last: "�ltimo",
                     next: "Siguiente",
                     previous: "Anterior"
                 },
                 aria: {
                     sortAscending: ": Activar para ordenar la columna de manera ascendente",
                     sortDescending: ": Activar para ordenar la columna de manera descendente"
                 }
             }
        });
    });
</script>
</body>
</html>
