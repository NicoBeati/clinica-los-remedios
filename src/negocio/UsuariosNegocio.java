package negocio;

import java.util.List;

import entidad.Usuarios;

public interface UsuariosNegocio {
	public boolean existeUsuario(Usuarios usuario);
	public Usuarios BuscarAdminPorDni(String DNI);
	public Usuarios BuscarUsuarioMedicoPorDni (String DNI);
	public boolean VerificarNombreUsuario(String usuario);
	public boolean VerificarUsuarioPorDni(String dni);
	public int  BuscarRegistroUsuarioPorDni(String dni);
	
	public boolean insertUsuarios(Usuarios usuario);
	
	public boolean borrarUsuarios(Usuarios usuario);
	
	public boolean ModificarAdminPorDni(Usuarios usuario);

	public boolean ModificarPwdUsuario(String usuario, String pwd);
	public boolean ModificarEstadoDeUsuario(String dni, String usuario, String email, String contrasena);
	public boolean ModificarUsuarioMedPorDni(Usuarios usuario);

	public List<Usuarios> readAllUsuarios();

	public Usuarios buscarUsuario(String nombreUsuario , String pwd);
	
}
