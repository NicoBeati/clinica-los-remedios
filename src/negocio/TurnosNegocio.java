package negocio;

import java.util.List;

import entidad.Especialidad;
import entidad.Especialistas;
import entidad.Pacientes;
import entidad.Provincia;
import entidad.ReporteMedico;
import entidad.ReportePaciente;
import entidad.Localidad;
import entidad.Turnos;
import entidad.Usuarios;

public interface TurnosNegocio {

	public boolean insertTurnos(Turnos turno);
	
	public boolean borrarTurnos(Turnos turno);

	public boolean ModificarTurnos(Turnos turnos);

	public boolean modificarTurno(String dni, int codTurno, String motivo, int estado);

	public List<Turnos> readAllTurnos();

	public List<Turnos> readAllTurnosPorDniEspecialista(String usuario);
	public List<Turnos> readAllTurnosPorDniPaciente(String usuario);
	
}
