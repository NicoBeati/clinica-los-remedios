package negocio;

import java.util.List;

import entidad.Localidad;

public interface LocalidadesNegocio {
	
	public List<Localidad> readAllLocalidades(String codProvincia);
	public Localidad buscarNombreLocalidad(String codLocalidad);
	
}
