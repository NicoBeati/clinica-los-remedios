package negocio;

import java.util.List;

import entidad.Especialidad;


public interface EspecialidadesNegocio {
	
	public List<Especialidad> readAllEspecialidad();
	
	public List<Especialidad> readEspecialidadByEspecialidadCodigo(String codigoEspecialidad);
	
	public Especialidad buscarEspecialidadPorCodigo(String codEspecialidad);
	
}
