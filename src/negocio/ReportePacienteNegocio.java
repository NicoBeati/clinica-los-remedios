package negocio;

import java.util.List;

import entidad.ReportePaciente;


public interface ReportePacienteNegocio {
	

	public List<ReportePaciente> filtrarReportePaciente(String dni, String especialidad);
	
}
