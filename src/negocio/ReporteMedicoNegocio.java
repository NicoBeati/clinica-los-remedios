package negocio;

import java.util.List;

import entidad.ReporteMedico;



public interface ReporteMedicoNegocio {
	
	public List<ReporteMedico> filtrarReporteMedico(String mes, String codEspecialidad);
	
}
