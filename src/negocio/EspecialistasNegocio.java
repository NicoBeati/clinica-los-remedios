package negocio;

import java.util.List;

import entidad.Especialistas;


public interface EspecialistasNegocio {
	
	public Especialistas BuscarEspecialistaPorDni(String DNI);

	
	public boolean insertEspecialistas(Especialistas especialista);
	
	public boolean insertHorarioEspecialistas(Especialistas especialista,String dia, String horario);
	
	public boolean borrarEspecialistas(Especialistas especialista);
	
	public boolean ModificarMedicoPorDni(Especialistas especialista);

	
	public List<Especialistas> readAllEspecialistas(String codigoEspecialidad);


	public List<Especialistas> readEspecialistasByEspecialidadCodigo(String codigoEspecialidad);

	
}
