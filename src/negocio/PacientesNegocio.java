package negocio;

import java.util.List;

import entidad.Pacientes;



public interface PacientesNegocio {
	
	public Pacientes BuscarPacientesPorDni(String DNI);
	
	public boolean insertPacientes(Pacientes paciente);
	
	public boolean borrarPacientes(Pacientes paciente);
	
	public boolean ModificarPacientePorDni(Pacientes pacientes);

	public List<Pacientes> readAllPacientes();
	


	
}
