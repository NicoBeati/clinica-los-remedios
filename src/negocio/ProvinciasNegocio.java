package negocio;

import java.util.List;

import entidad.Provincia;

public interface ProvinciasNegocio {
	
	public List<Provincia> readAllProvincias();
	public Provincia buscarNombreProvincia(String codProvincia);

}
