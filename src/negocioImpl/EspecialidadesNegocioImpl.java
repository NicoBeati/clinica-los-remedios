package negocioImpl;

import java.util.List;

import com.mysql.jdbc.Statement;

import dao.ClinicaDao;
import daoImpl.ClinicaDaoImpl;
import entidad.Especialidad;
import negocio.EspecialidadesNegocio;


public class EspecialidadesNegocioImpl implements EspecialidadesNegocio{
	
	ClinicaDao cdao = new ClinicaDaoImpl();
	

	//---------------------------------------LISTAR---------------------------------------//
	

	@Override
	public List<Especialidad> readAllEspecialidad() {
		return cdao.readAllEspecialidad();
	}

	
	//---------------------------------------BUSCAR---------------------------------------//
	

	@Override
	public List<Especialidad> readEspecialidadByEspecialidadCodigo(String codigoEspecialidad) {
		return cdao.readAllEspecialidad();
	}


	@Override
	public Especialidad buscarEspecialidadPorCodigo(String codEspecialidad) {
		return cdao.buscarEspecialidadPorCodigo(codEspecialidad);
	}
	
	
}
