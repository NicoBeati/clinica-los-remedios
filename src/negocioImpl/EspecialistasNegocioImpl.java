package negocioImpl;

import java.util.List;

import com.mysql.jdbc.Statement;

import dao.ClinicaDao;
import daoImpl.ClinicaDaoImpl;
import entidad.Especialistas;
import negocio.EspecialistasNegocio;


public class EspecialistasNegocioImpl implements EspecialistasNegocio{
	
	ClinicaDao cdao = new ClinicaDaoImpl();
	
	//---------------------------------------INSERTAR---------------------------------------//
	
	@Override
	public boolean insertEspecialistas(Especialistas especialista) {
		return cdao.insertEspecialistas(especialista);
	}

	
	@Override
	public boolean insertHorarioEspecialistas(Especialistas especialista, String dia, String horario) {
		return cdao.insertHorarioEspecialista(especialista, dia, horario);
	}
	
	//---------------------------------------BORRAR---------------------------------------//

	@Override
	public boolean borrarEspecialistas(Especialistas especialista) {
		return cdao.borrarEspecialistas(especialista);
	}

	
	//---------------------------------------MODIFICAR---------------------------------------//
	
	
	@Override
    public boolean ModificarMedicoPorDni(Especialistas especialista) {
        return cdao.ModificarMedicoPorDni(especialista);
	}
	

	//---------------------------------------LISTAR---------------------------------------//
	
	@Override
	public List<Especialistas> readAllEspecialistas(String codigoEspecialidad) {
		return cdao.readAllEspecialistas(codigoEspecialidad);
	}

	
	//---------------------------------------BUSCAR---------------------------------------//


	@Override
	public Especialistas BuscarEspecialistaPorDni(String DNI) {
		return cdao.BuscarEspecialistaPorDni(DNI);
	}
	

	@Override
	public List<Especialistas> readEspecialistasByEspecialidadCodigo(String codigoEspecialidad) {
		return cdao.readEspecialistasByEspecialidadCodigo(codigoEspecialidad);
	}

	

	

	

	
	
}
