package negocioImpl;

import java.util.List;

import com.mysql.jdbc.Statement;

import dao.ClinicaDao;
import daoImpl.ClinicaDaoImpl;
import entidad.Provincia;
import negocio.ProvinciasNegocio;


public class ProvinciasNegocioImpl implements ProvinciasNegocio{
	
	ClinicaDao cdao = new ClinicaDaoImpl();

	@Override
	public List<Provincia> readAllProvincias() {
		return cdao.readAllProvincias();
	}

	@Override
	public Provincia buscarNombreProvincia(String codProvincia) {
		return cdao.buscarNombreProvincia(codProvincia);
	}
	
	
}
