package negocioImpl;

import java.util.List;

import com.mysql.jdbc.Statement;

import dao.ClinicaDao;
import daoImpl.ClinicaDaoImpl;
import entidad.Usuarios;
import negocio.UsuariosNegocio;


public class UsuariosNegocioImpl implements UsuariosNegocio{
	
	ClinicaDao cdao = new ClinicaDaoImpl();
	
	
	//---------------------------------------VERIFICAR EXISTENCIA DE USUARIO LOGIN---------------------------------------//
	@Override
	public boolean existeUsuario(Usuarios usuario) {
		return cdao.existeUsuario(usuario);
	}	
			
			@Override
			public Usuarios buscarUsuario(String nombreUsuario , String pwd) {
				
				List<Usuarios> listaUsuarios = cdao.readAllUsuarios();
				
				for(Usuarios usu : listaUsuarios) {
					//con los strings tengo que utilizar el equales()
					if (usu.getUsuario().equals(nombreUsuario)  && usu.getContrasena_Usuarios().equals(pwd) ) {
						
						return usu;
					}
			    }
				
				return null;
			}
		@Override
		public boolean VerificarNombreUsuario(String usuario) {
			return cdao.VerificarNombreUsuario(usuario);
		}
		
		@Override
		public boolean VerificarUsuarioPorDni(String dni) {
			// TODO Auto-generated method stub
			return cdao.VerificarUsuarioPorDni(dni);
		}
		
		@Override
		public int  BuscarRegistroUsuarioPorDni(String dni){
			return cdao.BuscarRegistroUsuarioPorDni(dni);
		}
		
	//---------------------------------------INSERTAR---------------------------------------//
	

	@Override
	public boolean insertUsuarios(Usuarios usuario) {
		return cdao.insertUsuarios(usuario);
	}
	
	//---------------------------------------BORRAR---------------------------------------//

	@Override
	public boolean borrarUsuarios(Usuarios usuario) {
		return cdao.borrarUsuarios(usuario);
	}
	
	//---------------------------------------MODIFICAR---------------------------------------//
	
	@Override
	public boolean ModificarAdminPorDni(Usuarios usuario) {
		return cdao.ModificarAdminPorDni(usuario);
	}
	
	@Override
	public boolean ModificarPwdUsuario(String usuario, String pwd) {
		return cdao.ModificarPwdUsuario(usuario, pwd);
	}
	@Override
	public boolean ModificarEstadoDeUsuario(String dni, String usuario, String email, String contrasena) {
		return cdao.ModificarEstadoDeUsuario(dni, usuario, email, contrasena);
	}

	@Override
	public boolean ModificarUsuarioMedPorDni(Usuarios usuario) {
		return cdao.ModificarUsuarioMedPorDni(usuario);
	}
	
	//---------------------------------------LISTAR---------------------------------------//

	@Override
	public List<Usuarios> readAllUsuarios() {
		return cdao.readAllUsuarios();
	}
	
	//---------------------------------------BUSCAR---------------------------------------//
	

	@Override
	public Usuarios BuscarAdminPorDni(String DNI) {
		return cdao.BuscarAdminPorDni(DNI);
	}
	
	@Override
    public Usuarios BuscarUsuarioMedicoPorDni (String DNI) {
        return cdao.BuscarUsuarioMedicoPorDni(DNI);
    }

	
	

	
	
	
}
