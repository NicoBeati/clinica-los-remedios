package negocioImpl;

import java.util.List;

import com.mysql.jdbc.Statement;

import dao.ClinicaDao;
import daoImpl.ClinicaDaoImpl;
import entidad.Pacientes;




import negocio.PacientesNegocio;


public class PacientesNegocioImpl implements PacientesNegocio{
	
	ClinicaDao cdao = new ClinicaDaoImpl();
	
	
	
	//---------------------------------------INSERTAR---------------------------------------//
	

	@Override
	public boolean insertPacientes(Pacientes paciente) {
		return cdao.insertPacientes(paciente);
	}

	
	//---------------------------------------BORRAR---------------------------------------//


	@Override
	public boolean borrarPacientes(Pacientes paciente) {
		return cdao.borrarPacientes(paciente);
	}

	//---------------------------------------MODIFICAR---------------------------------------//
	

	@Override
	public boolean ModificarPacientePorDni(Pacientes pacientes) {
		return cdao.ModificarPacientePorDni(pacientes);
	}
	

	//---------------------------------------LISTAR---------------------------------------//
	

	@Override
	public List<Pacientes> readAllPacientes() {
		return cdao.readAllPacientes();
	}

	
	//---------------------------------------BUSCAR---------------------------------------//
	

	@Override
	public Pacientes BuscarPacientesPorDni(String DNI) {
		return cdao.BuscarPacientesPorDni(DNI);
	}

	


	

	

	

	
	
}
