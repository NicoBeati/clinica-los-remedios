package negocioImpl;

import java.util.List;

import com.mysql.jdbc.Statement;

import dao.ClinicaDao;
import daoImpl.ClinicaDaoImpl;
import entidad.Localidad;
import negocio.LocalidadesNegocio;


public class LocalidadesNegocioImpl implements LocalidadesNegocio{
	
	ClinicaDao cdao = new ClinicaDaoImpl();
	

	@Override
	public List<Localidad> readAllLocalidades(String codProvincia) {
		return cdao.readAllLocalidades(codProvincia);
	}


	@Override
	public Localidad buscarNombreLocalidad(String codLocalidad) {
		return cdao.buscarNombreLocalidad(codLocalidad);
	}
	
	
}
