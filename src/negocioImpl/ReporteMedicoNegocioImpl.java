package negocioImpl;

import java.util.List;

import com.mysql.jdbc.Statement;

import dao.ClinicaDao;
import daoImpl.ClinicaDaoImpl;
import entidad.ReporteMedico;
import negocio.ReporteMedicoNegocio;


public class ReporteMedicoNegocioImpl implements ReporteMedicoNegocio{
	
	ClinicaDao cdao = new ClinicaDaoImpl();
	
	
	//---------------------------------------Reportes---------------------------------------//
	
	@Override
	public List<ReporteMedico> filtrarReporteMedico(String mes, String codEspecialidad) {
		return cdao.filtrarReporteMedico(mes, codEspecialidad);
	}
	
}
