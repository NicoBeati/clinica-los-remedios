package negocioImpl;

import java.util.List;

import com.mysql.jdbc.Statement;

import dao.ClinicaDao;
import daoImpl.ClinicaDaoImpl;
import entidad.Turnos;
import negocio.TurnosNegocio;


public class TurnosNegocioImpl implements TurnosNegocio{
	
	ClinicaDao cdao = new ClinicaDaoImpl();
	
	
	//---------------------------------------INSERTAR---------------------------------------//

	@Override
	public boolean insertTurnos(Turnos turno) {
		return cdao.insertTurnos(turno);
	}
	
	//---------------------------------------BORRAR---------------------------------------//

	@Override
	public boolean borrarTurnos(Turnos turno) {
		return cdao.borrarTurnos(turno);
	}

	
	//---------------------------------------MODIFICAR---------------------------------------//
	
	@Override
	public boolean ModificarTurnos(Turnos turnos) {
		return cdao.ModificarTurnos(turnos);
	}
	
	@Override
	public boolean modificarTurno(String dni, int codTurno, String motivo, int estado) {
		return cdao.modificarTurno(dni, codTurno, motivo, estado);
	}

	//---------------------------------------LISTAR---------------------------------------//

	@Override
	public List<Turnos> readAllTurnos() {
		return cdao.readAllTurnos();
	}
	
	//---------------------------------------BUSCAR---------------------------------------//
	
	@Override
	public List<Turnos> readAllTurnosPorDniEspecialista(String usuario) {
		return cdao.readAllTurnosPorDniEspecialista(usuario);
	}
	
	@Override
	public List<Turnos> readAllTurnosPorDniPaciente(String usuario) {
		return cdao.readAllTurnosPorDniPaciente(usuario);
	}
	
	
}
