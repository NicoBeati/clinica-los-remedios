package negocioImpl;

import java.util.List;

import com.mysql.jdbc.Statement;

import dao.ClinicaDao;
import daoImpl.ClinicaDaoImpl;
import entidad.ReportePaciente;
import negocio.ReportePacienteNegocio;


public class ReportePacienteNegocioImpl implements ReportePacienteNegocio{
	
	ClinicaDao cdao = new ClinicaDaoImpl();

	//---------------------------------------Reportes---------------------------------------//
	

	@Override
	public List<ReportePaciente> filtrarReportePaciente(String dni, String especialidad) {
		return cdao.filtrarReportePaciente(dni, especialidad);
	}
	
	
}
