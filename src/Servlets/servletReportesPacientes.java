package Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Pacientes;
import entidad.ReportePaciente;
import negocioImpl.ReportePacienteNegocioImpl;

@WebServlet("/servletReportesPacientes")
public class servletReportesPacientes extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public servletReportesPacientes() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ReportePacienteNegocioImpl repopacneg = new ReportePacienteNegocioImpl();
        String dni = request.getParameter("txtDni");
        String especialidad = request.getParameter("txtEspecialidad");
        if(dni == "")
        {
        	dni = null;
        }
        if(especialidad == "")
        {
        	especialidad = null;
        }
        
        List<ReportePaciente> listaRepPac = null;
        
        if(especialidad != null || dni != null)
        {
        	listaRepPac = repopacneg.filtrarReportePaciente(dni, especialidad);
        }
        

        request.setAttribute("listaRepPac", listaRepPac);
        RequestDispatcher dispatcher = request.getRequestDispatcher("ReportesPacientes.jsp");
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
