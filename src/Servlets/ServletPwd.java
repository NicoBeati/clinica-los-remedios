package Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Usuarios;
import negocioImpl.UsuariosNegocioImpl;

/**
 * Servlet implementation class ServletPwd
 */
@WebServlet("/ServletPwd")
public class ServletPwd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletPwd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String Mensaje = "" ; 
		String usuario = request.getParameter("txtUsuario");
		String nuevaPwd  = request.getParameter("txtPwd");
		String repetirPwd = request.getParameter("txtPwdRepetir");
		
		//validar valores null de los inputs
		
//		if(usuario.isEmpty() || nuevaPwd.isEmpty() || repetirPwd.isEmpty()) {
//			Mensaje = "Complete todos los campos";
//			request.setAttribute("Mensaje", Mensaje);
//			RequestDispatcher ua = request.getRequestDispatcher("/ResetPwd.jsp");
//			ua.forward(request, response);
//			
//		}
		
		if(nuevaPwd.toString().equals(repetirPwd.toString())) {
			UsuariosNegocioImpl usuneg = new UsuariosNegocioImpl();
		
			boolean existe = usuneg.VerificarNombreUsuario(usuario);
			System.out.println("valor del existe"+ existe);
			if(existe==true) {
				System.out.println("el usuario a modificar existe");
				 if(usuneg.ModificarPwdUsuario(usuario, nuevaPwd)==true) {
					 Mensaje = "Contraseña modificada con exito";
						request.setAttribute("MensajeExito", Mensaje);
						RequestDispatcher ua = request.getRequestDispatcher("/ResetPwd.jsp");
						ua.forward(request, response);
				 }else {
					 Mensaje = "No se pudo realizar la modificación de contraseña";
						request.setAttribute("MensajeError", Mensaje);
						RequestDispatcher ua = request.getRequestDispatcher("/ResetPwd.jsp");
						ua.forward(request, response);
				 }
				
			}else {
				Mensaje = "El usuario indicado no existe";
				request.setAttribute("MensajeError", Mensaje);
				RequestDispatcher ua = request.getRequestDispatcher("/ResetPwd.jsp");
				ua.forward(request, response);
			}
			
		}else {
			Mensaje = "Las contraseñas ingresadas no coinciden";
			request.setAttribute("MensajeError", Mensaje);
			RequestDispatcher ua = request.getRequestDispatcher("/ResetPwd.jsp");
			ua.forward(request, response);
		}
		
		
		
		
		
		
		
		
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
