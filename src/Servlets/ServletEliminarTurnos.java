 package Servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Statement;

import entidad.Turnos;
import excepciones.ErrorEliminacion;
import negocio.TurnosNegocio;
import negocioImpl.TurnosNegocioImpl;


/**
 * Servlet implementation class ServletEliminarTurnos
 */
@WebServlet("/ServletEliminarTurnos")
public class ServletEliminarTurnos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletEliminarTurnos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//LISTAR Turnos//
    	if (request.getParameter("Param") != null) {
    		TurnosNegocioImpl turneg = new TurnosNegocioImpl();
            List<Turnos> ListaTurnos = turneg.readAllTurnos();
            
            request.setAttribute("listaTurnos", ListaTurnos);

            RequestDispatcher rd = request.getRequestDispatcher("/EliminarTurnos.jsp");
            rd.forward(request, response);
        }

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//BORRAR
		if (request.getParameter("btnEliminarTurnos") != null) {
			TurnosNegocio turneg = new TurnosNegocioImpl();
            Turnos tur = new Turnos();
            boolean estado = false;
              	tur.setCod_Turnos(Integer.parseInt(request.getParameter("Cod_Turno").toString()));
              	
              	try {
                    estado = turneg.borrarTurnos(tur);
                    if(estado!=true) {
                        ErrorEliminacion aux= new ErrorEliminacion();
                        throw aux;
                    }

                } catch (ErrorEliminacion e) {
                	System.out.println("Se produjo la siguiente excepci�n: " + e.getMessage());
                    e.printStackTrace();
                }
              	
            List<Turnos> ListaTurnos = turneg.readAllTurnos();

            request.setAttribute("listaTurnos", ListaTurnos);
            request.setAttribute("Agrego",estado);
             
            RequestDispatcher rd = request.getRequestDispatcher("/EliminarTurnos.jsp");
            rd.forward(request, response);
        }
		
		
		doGet(request, response);
	}

}
