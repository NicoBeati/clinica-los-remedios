package Servlets;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Especialidad;
import entidad.Especialistas;
import negocioImpl.EspecialidadesNegocioImpl;
import negocioImpl.EspecialistasNegocioImpl;
import negocioImpl.LocalidadesNegocioImpl;
import negocioImpl.ProvinciasNegocioImpl;
import negocioImpl.UsuariosNegocioImpl;
import entidad.Usuarios;
import excepciones.ErrorEliminacion;
import excepciones.ErrorEnInsertar;
import excepciones.ModificacionNoexitosa;
import negocio.EspecialidadesNegocio;
import negocio.EspecialistasNegocio;
import negocio.LocalidadesNegocio;
import negocio.ProvinciasNegocio;
import negocio.UsuariosNegocio;
import entidad.Provincia;
import entidad.Localidad;


@WebServlet("/servletCreaModEspecialista")
public class servletCreaModEspecialista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public servletCreaModEspecialista() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

//CARGAR DDLs AL INICIO
		if(request.getParameter("Param")!=null)
		{
			//Entra por haber hecho click en el hyperlink
			
			//carga ddl de Especialidades
			EspecialidadesNegocioImpl espeneg = new EspecialidadesNegocioImpl();
			ProvinciasNegocioImpl provneg = new ProvinciasNegocioImpl();
			LocalidadesNegocioImpl localneg = new LocalidadesNegocioImpl();
	        List<Especialidad> listaEsp = espeneg.readAllEspecialidad();
	        	//mando la lista al jsp
	        request.setAttribute("listaEsp", listaEsp);
	        
	        //carga ddl Provincias
	        List<Provincia> listaProv = provneg.readAllProvincias();
	        	//mando la lista al jsp
	        request.setAttribute("listaProv", listaProv);
	        
	        // carga ddl Localidades
	        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
        		//mando la lista al jsp
	        request.setAttribute("listaLoc", listaLoc);
	        RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioMedico.jsp");
		    rd.forward(request, response);
		    return; 
		}
		

//CREO VARIABLES
		String MensajeResu="";
		EspecialistasNegocioImpl medneg= new EspecialistasNegocioImpl();
		UsuariosNegocioImpl usuneg= new UsuariosNegocioImpl();
		ProvinciasNegocioImpl provneg= new ProvinciasNegocioImpl();
		LocalidadesNegocioImpl localneg= new LocalidadesNegocioImpl();

		EspecialidadesNegocioImpl especialneg = new EspecialidadesNegocioImpl();

		
		Especialistas medico = new Especialistas();
		Usuarios usuMedico = new Usuarios();
		Especialistas medicoE = new Especialistas();
		Usuarios usuMedicoE = new Usuarios();
		
		/*-----reemplazar por mensajes de error-----*/
		boolean siFormCompleto = false;
		boolean siModificacionExitosa = false;
		boolean siModificacionExitosaMed = false;
		boolean siModificacionExitosaUsu = false;
		boolean siModificacionExitosaEliminar = false;
		boolean siModificacionExitosaMedElim = false;
		boolean siModificacionExitosaUsuElim = false;
		
		//preguntar si es null antes de llamar a metodo
		if(request.getParameter("txtDni")!= null && !request.getParameter("txtDni").isEmpty() && 
				request.getParameter("txtNombre")!= null && !request.getParameter("txtNombre").isEmpty() &&
				request.getParameter("txtApellido")!= null && !request.getParameter("txtApellido").isEmpty() && 
				request.getParameter("rdBtnSexo")!= null && !request.getParameter("rdBtnSexo").isEmpty() &&
				request.getParameter("txtNacionalidad")!= null && !request.getParameter("txtNacionalidad").isEmpty() && 
				request.getParameter("dateFechaNac")!= null && !request.getParameter("dateFechaNac").isEmpty() &&
				request.getParameter("txtDireccion")!= null && !request.getParameter("txtDireccion").isEmpty() &&
				request.getParameter("Localidades")!= null && !request.getParameter("Localidades").isEmpty() &&
				request.getParameter("Provincias")!= null && !request.getParameter("Provincias").isEmpty() &&
				request.getParameter("txtEmail")!= null && !request.getParameter("txtEmail").isEmpty() &&
				request.getParameter("txtTelefono")!= null && !request.getParameter("txtTelefono").isEmpty() && 
				request.getParameter("Especialidades")!= null && !request.getParameter("Especialidades").isEmpty() && 
				request.getParameter("txtContrasenia")!= null && !request.getParameter("txtContrasenia").isEmpty() && 
				request.getParameter("txtConfContrasenia")!= null && !request.getParameter("txtConfContrasenia").isEmpty())
		{
			siFormCompleto = true;
		}
	
		if (siFormCompleto== true) {
			
			// GUARDANDO ESPECIALISTA
			medico.setDNI_Especialistas(request.getParameter("txtDni"));
			medico.setNombre_Especialistas(request.getParameter("txtNombre"));
			medico.setApellido_Especialistas(request.getParameter("txtApellido"));
			medico.setSexo_Especialistas(request.getParameter("rdBtnSexo").charAt(0));
			medico.setNacionalidad_Especialistas(request.getParameter("txtNacionalidad"));
			medico.setFechaNac_Especialistas(request.getParameter("dateFechaNac"));
			medico.setDireccion_Especialistas(request.getParameter("txtDireccion"));
			medico.setEmail_Especialistas(request.getParameter("txtEmail"));
			medico.setTelefono_Especialistas(request.getParameter("txtTelefono"));
			medico.setEstado_Especialistas(1);
				//busco y cargo obj prov
			Provincia prov = new Provincia ();
			prov = provneg.buscarNombreProvincia(request.getParameter("Provincias"));
			medico.setProvincia_nombreProvincia(prov.getNombreProvincia());
				//busco y cargo obj prov
			Localidad loc = new Localidad ();
			loc = localneg.buscarNombreLocalidad(request.getParameter("Localidades"));		
			medico.setLocalidad_nombreLocalidad(loc.getNombreLocalidad());
			
			Especialidad espe = new Especialidad();
			espe = especialneg.buscarEspecialidadPorCodigo(request.getParameter("Especialidades"));
			medico.setEspecialidadCodigo(espe.getCodEspecialidad());
			
			
			// GUARDANDO USUARIO 
			usuMedico.setDni(request.getParameter("txtDni"));
			usuMedico.setUsuario( request.getParameter("txtDni"));
			usuMedico.setContrasena_Usuarios(request.getParameter("txtContrasenia"));
			usuMedico.setEmail_Usuarios(request.getParameter("txtEmail"));
			usuMedico.setTipo_Usuarios('E');
		
		} 
		/*else {
			MensajeResu="Complete todos los campos";
			request.setAttribute("MensajeError", MensajeResu);
			RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioMedico.jsp");
		    rd.forward(request, response);
		}*/
		
	
//SI ENTRA POR HACER CLICK EN GUARDAR
		if (request.getParameter("btnGuardarMed")!=null && siFormCompleto== true && request.getParameter("txtContrasenia").equals(request.getParameter("txtConfContrasenia")))
		{
			String MensajeResu1="";
	        try {
	        	
				siModificacionExitosaMed = medneg.insertEspecialistas(medico);
				siModificacionExitosaUsu = usuneg.insertUsuarios(usuMedico);
				
				if(siModificacionExitosaMed!=true || siModificacionExitosaUsu!=true) {
					
					
					MensajeResu1="Error al Guardar Especialista";
					request.setAttribute("MensajeError", MensajeResu1);
				
					
					ErrorEnInsertar inser = new ErrorEnInsertar();
					throw inser;	
				} else {
					MensajeResu1="Guardado Exitoso";
					request.setAttribute("MensajeExito", MensajeResu1);
				}
			}
			catch (ErrorEnInsertar e) {
				System.out.println("Se produjo la siguiente excepción: " + e.getMessage());
				e.printStackTrace();
			
			} 
	        
	      //VUELVO A CARGAR EL DDL
			EspecialidadesNegocioImpl espeneg2 = new EspecialidadesNegocioImpl();
	        
	        	//carga ddl especialidades
	        List<Especialidad> listaEsp = espeneg2.readAllEspecialidad();
	        	//mando la lista al jsp
	        request.setAttribute("listaEsp", listaEsp);
	        
	        	//carga ddl Provincias
	        List<Provincia> listaProv = provneg.readAllProvincias();
	        	//mando la lista al jsp
	        request.setAttribute("listaProv", listaProv);
	        
	        	// carga ddl Localidades
	        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
	    		//mando la lista al jsp
	        request.setAttribute("listaLoc", listaLoc);
	        ////
	        
	        RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioMedico.jsp");
		    rd.forward(request, response);
		    return;
		} // PARA VER SI LAS CONTRASEÑAS SON IGUALES
		/*if (request.getParameter("btnGuardarMed")!=null && !request.getParameter("txtContrasenia").equals(request.getParameter("txtConfContrasenia"))) {
			String MensajeResu4="";
			
        	MensajeResu4="Error, las contraseñas deben ser iguales";
			request.setAttribute("MensajeError", MensajeResu4);
			
			  
		      //VUELVO A CARGAR EL DDL
				EspecialidadesNegocioImpl espeneg2 = new EspecialidadesNegocioImpl();
		        
		        	//carga ddl especialidades
		        List<Especialidad> listaEsp = espeneg2.readAllEspecialidad();
		        	//mando la lista al jsp
		        request.setAttribute("listaEsp", listaEsp);
		        
		        	//carga ddl Provincias
		        List<Provincia> listaProv = provneg.readAllProvincias();
		        	//mando la lista al jsp
		        request.setAttribute("listaProv", listaProv);
		        
		        	// carga ddl Localidades
		        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
		    		//mando la lista al jsp
		        request.setAttribute("listaLoc", listaLoc);
		        ////
			
			RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioMedico.jsp");
		    rd.forward(request, response);
		    return;
        }*/
	
//SI ENTRA POR HACER CLICK EN ELIMINAR ----> anda
		if (request.getParameter("btnEliminarMed")!=null && !request.getParameter("txtDni").isEmpty())
		{

			usuMedicoE.setDni(request.getParameter("txtDni"));
			medicoE.setDNI_Especialistas(request.getParameter("txtDni"));
			String MensajeResu2="";
			
			try {
				siModificacionExitosaMedElim = medneg.borrarEspecialistas(medicoE);
				siModificacionExitosaUsuElim = usuneg.borrarUsuarios(usuMedicoE);
				if(siModificacionExitosaMedElim!=true && siModificacionExitosaUsuElim !=true) {
					
					MensajeResu2="Error al Eliminar Especialista";
					request.setAttribute("MensajeError", MensajeResu2);
					ErrorEliminacion eli= new ErrorEliminacion();
					throw eli;
				} else {
					MensajeResu2="Eliminado Exitoso";
					request.setAttribute("MensajeExito", MensajeResu2);
				}
			}
			catch (ErrorEliminacion e) {
				System.out.println("Se produjo la siguiente excepción: " + e.getMessage());
				e.printStackTrace();
			}
			
			//VUELVO A CARGAR EL DDL
			EspecialidadesNegocioImpl espeneg2 = new EspecialidadesNegocioImpl();
	        
	        	//carga ddl especialidades
	        List<Especialidad> listaEsp = espeneg2.readAllEspecialidad();
	        	//mando la lista al jsp
	        request.setAttribute("listaEsp", listaEsp);
	        
	        	//carga ddl Provincias
	        List<Provincia> listaProv = provneg.readAllProvincias();
	        	//mando la lista al jsp
	        request.setAttribute("listaProv", listaProv);
	        
	        	// carga ddl Localidades
	        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
	    		//mando la lista al jsp
	        request.setAttribute("listaLoc", listaLoc);
	        ////
			
			RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioMedico.jsp");
		    rd.forward(request, response);
		    return;
		}
		
				
//SI ENTRA POR HACER CLICK EN BUSCAR
	    if (request.getParameter("btnBuscarMed") != null && !request.getParameter("txtUsuario").isEmpty()) {
            
	    	EspecialidadesNegocioImpl espeneg3 = new EspecialidadesNegocioImpl();
            ProvinciasNegocioImpl provneg2 = new ProvinciasNegocioImpl();
            LocalidadesNegocioImpl localneg2 = new LocalidadesNegocioImpl();
            EspecialistasNegocioImpl medneg2 = new EspecialistasNegocioImpl();
            UsuariosNegocioImpl usuneg2 = new UsuariosNegocioImpl();
            
		    
		    //TRAIGO LA DATA Y LA POPULO A LOS CONTROLES
            Especialistas esp = new Especialistas();
            Usuarios usu = new Usuarios();
            String dni = request.getParameter("txtUsuario");
            
            usu = usuneg2.BuscarUsuarioMedicoPorDni(dni);          
            esp = medneg2.BuscarEspecialistaPorDni(dni);
            
            if(!usu.getUsuario().isEmpty() || !esp.getNombre_Especialistas().isEmpty())
            {
            	request.setAttribute("dni", esp.getDNI_Especialistas());
                request.setAttribute("nombre", esp.getNombre_Especialistas());
                request.setAttribute("apellido", esp.getApellido_Especialistas());
                request.setAttribute("sexo", esp.getSexo_Especialistas());
                request.setAttribute("nacionalidad", esp.getNacionalidad_Especialistas());
                request.setAttribute("fecha_nac", esp.getFechaNac_Especialistas());
                request.setAttribute("direccion", esp.getDireccion_Especialistas());
                request.setAttribute("localidad", esp. getnombreLocalidad_Especialistas());
                request.setAttribute("provincia", esp.getProvincia_Especialistas());
                request.setAttribute("email", esp.getEmail_Especialistas());
                request.setAttribute("telefono", esp.getTelefono_Especialistas());
                request.setAttribute("contrasenia", usu.getContrasena_Usuarios());
                request.setAttribute("conf_contrasenia", usu.getContrasena_Usuarios());
                request.setAttribute("especialidad", esp.getEspecialidadCodigo());
            } else {
            	String MensajeResu6="";
            	MensajeResu6="Especialista no encontrado";
				request.setAttribute("MensajeErrorBuscar", MensajeResu6);
            }

       
          //VUELVO A CARGAR EL DDL
    		EspecialidadesNegocioImpl espeneg2 = new EspecialidadesNegocioImpl();
            
            	//carga ddl especialidades
            List<Especialidad> listaEsp = espeneg2.readAllEspecialidad();
            	//mando la lista al jsp
            request.setAttribute("listaEsp", listaEsp);
            
            	//carga ddl Provincias
            List<Provincia> listaProv = provneg.readAllProvincias();
            	//mando la lista al jsp
            request.setAttribute("listaProv", listaProv);
            
            	// carga ddl Localidades
            List<Localidad> listaLoc = localneg.readAllLocalidades(null);
        		//mando la lista al jsp
            request.setAttribute("listaLoc", listaLoc);
            ////

         }
	    
//SI ENTRA POR HACER CLICK EN MODIFICAR
        if (request.getParameter("btnModificarMed")!=null && request.getParameter("txtDni") != null && !request.getParameter("txtDni").isEmpty())
            {
        	//CARGO DDLs
	        	//carga ddl de Especialidades
        	EspecialistasNegocioImpl medneg3 = new EspecialistasNegocioImpl();
        	
        	// busco los datos del medico
        		medicoE = medneg3.BuscarEspecialistaPorDni(request.getParameter("txtDni"));
        		
        	// cargo los datos del medico para madar la data a la Db	
    			medicoE.setNombre_Especialistas(request.getParameter("txtNombre"));
    			medicoE.setApellido_Especialistas(request.getParameter("txtApellido"));
    			medicoE.setSexo_Especialistas(request.getParameter("rdBtnSexo").charAt(0));
    			medicoE.setNacionalidad_Especialistas(request.getParameter("txtNacionalidad"));
    			medicoE.setFechaNac_Especialistas(request.getParameter("dateFechaNac"));
    			medicoE.setDireccion_Especialistas(request.getParameter("txtDireccion"));
    			medicoE.setEmail_Especialistas(request.getParameter("txtEmail"));
    			medicoE.setTelefono_Especialistas(request.getParameter("txtTelefono"));    			
    			//busco y cargo obj prov
    			Provincia prov = new Provincia ();
    			prov = provneg.buscarNombreProvincia(request.getParameter("Provincias"));
    			medicoE.setProvincia_nombreProvincia(prov.getNombreProvincia());
    				//busco y cargo obj prov
    			Localidad loc = new Localidad ();
    			loc = localneg.buscarNombreLocalidad(request.getParameter("Localidades"));		
    			medicoE.setLocalidad_nombreLocalidad(loc.getNombreLocalidad());
    				//busco y cargo obj esp
    			Especialidad espe = new Especialidad();
    			espe = especialneg.buscarEspecialidadPorCodigo(request.getParameter("Especialidades"));
    			medicoE.setEspecialidadCodigo(espe.getCodEspecialidad());
    			
    			
    			// GUARDANDO USUARIO 
    			usuMedico.setDni(request.getParameter("txtDni"));
    			usuMedico.setUsuario( request.getParameter("txtDni"));
    			usuMedico.setContrasena_Usuarios(request.getParameter("txtContrasenia"));
    			usuMedico.setEmail_Usuarios(request.getParameter("txtEmail"));
    			usuMedico.setTipo_Usuarios('E');
                
    			
    			String MensajeResu3="";
    	        try {
    	        	
    				siModificacionExitosaMed = medneg.ModificarMedicoPorDni(medicoE);//update
    				siModificacionExitosaUsu = usuneg.ModificarUsuarioMedPorDni(usuMedico);//update
    				
    				if(siModificacionExitosaMed!=true && siModificacionExitosaUsu!=true) {
    					MensajeResu3="Error al Guardar Especialista";
    					request.setAttribute("MensajeError", MensajeResu3);
    					RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioMedico.jsp");
    				    rd.forward(request, response);
    					
    					ErrorEnInsertar inser = new ErrorEnInsertar();
    					throw inser;	
    				} else {
    					MensajeResu3="Guardado Exitoso";
    					request.setAttribute("MensajeExito", MensajeResu3);
    				}
    			}
    			catch (ErrorEnInsertar e) {
    				System.out.println("Se produjo la siguiente excepción: " + e.getMessage());
    				e.printStackTrace();
    			}
    	        
    			//VUELVO A CARGAR EL DDL
    			EspecialidadesNegocioImpl espeneg2 = new EspecialidadesNegocioImpl();
    	        
    	        	//carga ddl especialidades
    	        List<Especialidad> listaEsp = espeneg2.readAllEspecialidad();
    	        	//mando la lista al jsp
    	        request.setAttribute("listaEsp", listaEsp);
    	        
    	        	//carga ddl Provincias
    	        List<Provincia> listaProv = provneg.readAllProvincias();
    	        	//mando la lista al jsp
    	        request.setAttribute("listaProv", listaProv);
    	        
    	        	// carga ddl Localidades
    	        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
    	    		//mando la lista al jsp
    	        request.setAttribute("listaLoc", listaLoc);
    	        ////
    			
    			RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioMedico.jsp");
    		    rd.forward(request, response); 
    		    return;
            }
        
      //VUELVO A CARGAR EL DDL
		EspecialidadesNegocioImpl espeneg2 = new EspecialidadesNegocioImpl();
        
        	//carga ddl especialidades
        List<Especialidad> listaEsp = espeneg2.readAllEspecialidad();
        	//mando la lista al jsp
        request.setAttribute("listaEsp", listaEsp);
        
        	//carga ddl Provincias
        List<Provincia> listaProv = provneg.readAllProvincias();
        	//mando la lista al jsp
        request.setAttribute("listaProv", listaProv);
        
        	// carga ddl Localidades
        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
    		//mando la lista al jsp
        request.setAttribute("listaLoc", listaLoc);
        ////
        
	    RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioMedico.jsp");
        rd.forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	}

}

