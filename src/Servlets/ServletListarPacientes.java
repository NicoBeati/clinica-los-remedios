package Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Pacientes;
import negocioImpl.PacientesNegocioImpl;

/**
 * Servlet implementation class ServletListarPacientes
 */
@WebServlet("/ServletListarPacientes")
public class ServletListarPacientes extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletListarPacientes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//LISTAR PACIENTE//
    	if (request.getParameter("Param") != null) {
    		PacientesNegocioImpl pacneg = new PacientesNegocioImpl();
            List<Pacientes> ListaPacientes = pacneg.readAllPacientes();

            request.setAttribute("listaPacientes", ListaPacientes);

            RequestDispatcher rd = request.getRequestDispatcher("/ListarPacientes.jsp");
            rd.forward(request, response);
        }
	}
		

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
