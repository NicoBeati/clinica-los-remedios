package Servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Calendar;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Statement;

import daoImpl.Conexion;
import entidad.Especialidad;
import entidad.Especialistas;
import entidad.ExDxH;
import entidad.Pacientes;
import entidad.Turnos;
import entidad.Usuarios;
import excepciones.DniNoencontrado;
import excepciones.ErrorEnInsertar;
import negocioImpl.ExDxHNegocioImpl;
import negocioImpl.PacientesNegocioImpl;
import negocioImpl.TurnosNegocioImpl;



/**
 * Servlet implementation class ServletsPedirTurno
 */
@WebServlet("/ServletsPedirTurno")
public class ServletsPedirTurno extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletsPedirTurno() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	boolean bandera=false;
    	
    	//LISTAR TURNOS LIBRES
    	if (request.getParameter("Param") != null) {
    		TurnosNegocioImpl turneg = new TurnosNegocioImpl();

    	    List<Turnos>  turnosListLIBRES = turneg.readAllTurnos();

    	    request.setAttribute("turnosListLIBRES",  turnosListLIBRES); 
    	    RequestDispatcher rd = request.getRequestDispatcher("/PedirTurno.jsp");
    	    rd.forward(request, response);
    	    
        }
    	
    	//GUARDAR (MODIFICAR) TURNOS
        if (request.getParameter("btnguardar") != null) {
        	PacientesNegocioImpl pacneg = new PacientesNegocioImpl();
        	TurnosNegocioImpl turneg = new TurnosNegocioImpl();
        	String dni= request.getParameter("dni_paciente");
            Pacientes dnipaciente=pacneg.BuscarPacientesPorDni(dni);
            Turnos turno = new Turnos();
            System.out.println("dni dentro del servlets que viene de buscar"+dnipaciente.getDNI_Pacientes() + "dni del jsp" + dni);
            boolean estado = false;
            System.out.println("localidad "+dnipaciente.getLocalidad() + "provincia" + dnipaciente.getProvincia());
           
            if(dni.equals(dnipaciente.getDNI_Pacientes()))
            {
            	int asistencia=Integer.parseInt(request.getParameter("Asistencia_turno"));
            	int codTurnos=Integer.parseInt(request.getParameter("Id_turno"));
            	
            	turno.setDni_Paciente_Turnos(dni);
            	turno.setAsistencia_Turnos(asistencia);
            	turno.setCod_Turnos(codTurnos);

            	/*OCUPADO:0
            	 LIBRE:1
            	 AUSENTE: 2
            	 PRESENTE:3 estos son los valores que se envian a la base de datos segun el estado del turno
            	 motivo de consulta lo mandamos vacio porque despues eso lo completa el doctor*/
            	   
            	try {
                   bandera= turneg.ModificarTurnos(turno);
                   if(bandera !=true) {
                	   ErrorEnInsertar inser=new ErrorEnInsertar();
                	   throw inser;

                   }
            	
                   
                } catch (ErrorEnInsertar  e) {
                    System.out.println("Se produjo la siguiente excepci�n: " + e.getMessage());
                    e.printStackTrace();
                }
 
            }
            TurnosNegocioImpl turneg2 = new TurnosNegocioImpl();	
            System.out.println("estado"+bandera);
    	    List<Turnos>  turnosListLIBRES = turneg2.readAllTurnos();

    	    request.setAttribute("turnosListLIBRES",  turnosListLIBRES); 
     
            request.setAttribute("Agrego",bandera);
            
            RequestDispatcher rd = request.getRequestDispatcher("/PedirTurno.jsp");
            rd.forward(request, response);
        
         }
      
        
        //GENERAR (CREAR) TURNOS
        if (request.getParameter("btnGenerarTurnos") != null) {
            Calendar calendar = Calendar.getInstance();
            Turnos turno = new Turnos();
            boolean estado ; 
            ExDxHNegocioImpl ExDxHneg = new ExDxHNegocioImpl();
            TurnosNegocioImpl turneg = new TurnosNegocioImpl();
            String txtAnioParam = request.getParameter("txtAnio");
            String ddlMesParam = request.getParameter("ddlMes");
            
            if (txtAnioParam != null && ddlMesParam != null) {
                int anio = Integer.parseInt(txtAnioParam);
                int mes = Integer.parseInt(ddlMesParam);
            calendar.set(Calendar.MONTH, mes);
            calendar.set(anio, mes, 1); // Establecer el a�o y mes
           

            List<ExDxH> listaExDxH = ExDxHneg.readAllExDxHPorDniEspecialista(request.getParameter("txtDni"));
            int ultimoDiaMes = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

            for (int dia = 1; dia <= ultimoDiaMes; dia++) {
                calendar.set(anio, mes, dia);
                int diaSemana = calendar.get(Calendar.DAY_OF_WEEK) - 1;
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                String fechaFormateada = dateFormat.format(calendar.getTime());

                for (ExDxH exDxH : listaExDxH) {
                    if (diaSemana == exDxH.getId_Dias_EXDXH()) {
                        turno.setDni_Especialista_Turnos(exDxH.getEspecialistas());
                        turno.setCod_Especialidad_Turnos(exDxH.getEspecialidad());
                        turno.setDni_Paciente_Turnos("00000000");
                        turno.setAsistencia_Turnos(1);
                        turno.setMotivo_Consulta_Turnos("-");
                        turno.setFecha_Turnos(fechaFormateada);
                        turno.setId_Dia_Turnos(exDxH.getId_Dias_EXDXH());
                        turno.setId_Hora_Turnos(exDxH.getId_Horas_EXDXH());
                        turno.setDia_Turnos("-");
                        turno.setHorario_Turnos("-");
                        
                        try {
                        	turneg.insertTurnos(turno);
                        	
                           }
                        catch (Exception e) {
                        	System.out.println("Se produjo una excepci�n ");
                            e.printStackTrace();
                        }
                    	
                    }
                }
            }
            estado = true; 
            request.setAttribute("Agrego", estado);
            
            }
            
            RequestDispatcher rd = request.getRequestDispatcher("/GenerarTurnosMensual.jsp");
            rd.forward(request, response); 
        }
        
        
        
    }
        
    


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}
	
	

}
