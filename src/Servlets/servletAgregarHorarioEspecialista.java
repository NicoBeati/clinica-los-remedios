package Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Especialidad;
import entidad.Especialistas;
import entidad.Usuarios;
import excepciones.CamposIncompletos;
import excepciones.DniNoencontrado;
import excepciones.ModificacionNoexitosa;
import negocio.EspecialistasNegocio;
import negocioImpl.EspecialistasNegocioImpl;


@WebServlet("/servletAgregarHorarioEspecialista")
public class servletAgregarHorarioEspecialista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public servletAgregarHorarioEspecialista() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("paso1");
		EspecialistasNegocio cneg = new EspecialistasNegocioImpl();
        Especialistas esp = new Especialistas();
        boolean siModificacionExitosa = false;
        String DNI="";
        
		//BUSCAR//
        if (request.getParameter("btnBuscarMed") != null) {
            
            //boolean estado = false;
            System.out.println("paso2");
        	
        	
        	if(!request.getParameter("txtDni").isEmpty()){  
            	System.out.println("paso3"); 
            	
                	try {
                		
                    	DNI = request.getParameter("txtDni");
                    	System.out.println("paso4"+ DNI);
                    	esp = cneg.BuscarEspecialistaPorDni(DNI); 
                    	
                    	if(esp==null) {
                    		DniNoencontrado aux=new DniNoencontrado();
                    		throw aux;
                    	}

                    } catch (DniNoencontrado e) {
                    	System.out.println("Se produjo una excepci�n: " + e.getMessage());
                        e.printStackTrace();
                    } 


            }


        	

            System.out.println("paso6");
            request.setAttribute("esp", esp);
            request.setAttribute("txtDni1", DNI);

            RequestDispatcher rd = request.getRequestDispatcher("/AgregarHorarioEspecialista.jsp");
            rd.forward(request, response);
            return;
        }
		
		//AGREGAR HORARIO
        System.out.println("paso1");
        if (request.getParameter("btnAgregarHorario") != null) {
        	System.out.println("paso2");
        	try {
        		String dia=request.getParameter("Dias");
            	String horario=request.getParameter("Horario");
            	DNI = request.getParameter("txtDni");
            	esp = cneg.BuscarEspecialistaPorDni(DNI);
            	siModificacionExitosa = cneg.insertHorarioEspecialistas(esp, dia, horario);
            	ModificacionNoexitosa aux2= new ModificacionNoexitosa();
            	throw aux2;
            	
            }  
        	catch (ModificacionNoexitosa e) {
        		System.out.println("Se produjo una excepci�n: " + e.getMessage());
                e.printStackTrace();
            }

        	System.out.println("paso6");
        	request.setAttribute("Agrego", siModificacionExitosa);
            RequestDispatcher rd = request.getRequestDispatcher("/AgregarHorarioEspecialista.jsp");
            rd.forward(request, response);
        }
		
        	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
