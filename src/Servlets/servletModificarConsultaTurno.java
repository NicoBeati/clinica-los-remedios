package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entidad.Turnos;
import negocioImpl.TurnosNegocioImpl;

@WebServlet("/servletModificarConsultaTurno")
public class servletModificarConsultaTurno extends HttpServlet {
    private TurnosNegocioImpl turneg;

    public void init() throws ServletException {
        turneg = new TurnosNegocioImpl();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Redirige al formulario de modificación
        RequestDispatcher dispatcher = request.getRequestDispatcher("ModificarTurno.jsp");
        dispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String botonModificar = request.getParameter("Modificar");
        String usuarioMedico = (String) request.getSession().getAttribute("UsuarioMedico");
        if (botonModificar != null) {
            String dniEspecialista = (String) request.getSession().getAttribute("UsuarioMedico");
            String codigoTurno = request.getParameter("codigoTurno");
            String motivoTurno = request.getParameter("motivoTurno");
            String estadoTurno = request.getParameter("estadoTurno");

            // Llama a la función modificarTurno y pasa los parámetros necesarios
            turneg.modificarTurno(dniEspecialista, Integer.parseInt(codigoTurno), motivoTurno, Integer.parseInt(estadoTurno));

            List<Turnos> turnosList = turneg.readAllTurnosPorDniEspecialista(usuarioMedico);
            request.setAttribute("turnosList", turnosList);
            // Redirige al servletVistaMedico
            RequestDispatcher dispatcher = request.getRequestDispatcher("VistaMedico.jsp");
            dispatcher.forward(request, response);
        }
    }
}
