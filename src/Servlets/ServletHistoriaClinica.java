package Servlets;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entidad.Turnos;
import negocio.TurnosNegocio;
import negocioImpl.TurnosNegocioImpl;

@WebServlet("/ServletHistoriaClinica")
public class ServletHistoriaClinica extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private TurnosNegocio turneg;

    public ServletHistoriaClinica() {
        super();
        turneg = new TurnosNegocioImpl();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String dni = request.getParameter("dni");
        List<Turnos> turnosList = turneg.readAllTurnosPorDniPaciente(dni);
        request.setAttribute("turnosPList", turnosList);
        request.getRequestDispatcher("HistoriaClinica.jsp").forward(request, response);
        
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
