package Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Usuarios;
import negocioImpl.UsuariosNegocioImpl;
//import negocioImpl.ClinicaNegocioImpl;

/**
 * Servlet implementation class pluginUsuario
 */
@WebServlet("/ServletLogin")
public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public ServletLogin() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String nombre = request.getParameter("txtUsuario");
		String contrasena = request.getParameter("txtPwd");
       
		// request.setAttribute("nombre", Nombre);
		// request.setAttribute("apellido", Contrasena);

		String Mensaje = "No tiene datos";

		// VALIDACION DE CAMPOS

		if ((nombre == null || nombre.isEmpty()) && (contrasena == null || nombre.isEmpty())) {
			Mensaje = "Ingrese un usuario y contraseña";

		} else if (nombre == null || nombre.isEmpty()) {
			Mensaje = "Ingrese un usuario valido";

		} else if (contrasena == null || contrasena.isEmpty()) {
			Mensaje = "Ingrese una contraseña valida";

			
		}

		if ((nombre != null && !nombre.isEmpty()) && (contrasena != null && !contrasena.isEmpty())) {

			UsuariosNegocioImpl usuneg = new UsuariosNegocioImpl();
			// Usuarios loginUsuario = new Usuarios("434234", "Mellchu",
			// "mcarraro@gmail.com", "nose", 'a',1);
			
			Usuarios loginUsuario = usuneg.buscarUsuario(nombre, contrasena);
			if (loginUsuario == null) {
				Mensaje = "Usuario y/o contraseña incorrectos";
				

				request.setAttribute("MensajeServeletLogin", Mensaje);

				RequestDispatcher rd = request.getRequestDispatcher("/Login.jsp");
				rd.forward(request, response);
			} else if(loginUsuario.getEstado_Usuarios()!=1) {
		
				Mensaje = "el usuario es : " + loginUsuario.getEmail_Usuarios();
				nombre = "";
				contrasena = ""; 
				
				if (loginUsuario.getTipo_Usuarios() == 'A') {
					request.getSession().setAttribute("UsuarioAdmin", loginUsuario.getUsuario());
					RequestDispatcher ua = request.getRequestDispatcher("/HomeAdmin.jsp");
					ua.forward(request, response);
				}else if (loginUsuario.getTipo_Usuarios() == 'E') {
					 request.getSession().setAttribute("UsuarioMedico", loginUsuario.getUsuario());
					 RequestDispatcher um = request.getRequestDispatcher("/servletVistaMedico");
					 um.forward(request, response);
				}
			}
			// request.setAttribute("MensajeServeletLogin", Mensaje);

			return;
		}


		request.setAttribute("MensajeServeletLogin", Mensaje);


		RequestDispatcher rd = request.getRequestDispatcher("/Login.jsp");
		rd.forward(request, response);

		return;
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
