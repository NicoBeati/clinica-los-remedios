package Servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Especialidad;
import entidad.ReporteMedico;
import negocioImpl.EspecialidadesNegocioImpl;
import negocioImpl.ReporteMedicoNegocioImpl;


@WebServlet("/servletReportesMedico")
public class servletReportesMedico extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public servletReportesMedico() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EspecialidadesNegocioImpl espeneg = new EspecialidadesNegocioImpl();
		ReporteMedicoNegocioImpl repomedneg = new ReporteMedicoNegocioImpl();
		
		//CARGAR DDL
		if(request.getParameter("Param")!=null)
		{
			//Entra por haber hecho click en el hyperlink
	        List<Especialidad> listaEsp = espeneg.readAllEspecialidad();
        	//mando la lista al jsp
	        request.setAttribute("listaEsp", listaEsp);
	        
	        //me vuelvo al jsp
	        RequestDispatcher rd = request.getRequestDispatcher("/ReportesMedicos.jsp");
	        rd.forward(request, response);
	        return;
		}
		
		// PREGUNTO SI LOS 2 DDLs NO ESTAN EN NULL
		boolean siDdlsSeleccionadas = false;
		if(request.getParameter("Especialidades")!= null && !request.getParameter("Especialidades").isEmpty() &&
				request.getParameter("mes")!= null && !request.getParameter("mes").isEmpty()){
				
			siDdlsSeleccionadas=true;
		}
		
		//Si hago click en boton filtrar
		if(request.getParameter("btnFiltrar")!= null && siDdlsSeleccionadas==true) {
			
			//CARGAR DDL
			List<Especialidad> listaEsp = espeneg.readAllEspecialidad();
        	//mando la lista al jsp
	        request.setAttribute("listaEsp", listaEsp);
			
	        //CARGO TABLA
			List<ReporteMedico> listaRepMed = repomedneg.filtrarReporteMedico(request.getParameter("mes").toString(), request.getParameter("Especialidades").toString());
			
			//me falta mandar la data al jsp y configurar para que se vea
			request.setAttribute("listaRepMed", listaRepMed);
			RequestDispatcher rd = request.getRequestDispatcher("/ReportesMedicos.jsp");
	        rd.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
