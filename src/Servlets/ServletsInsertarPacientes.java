package Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Pacientes;
import negocio.PacientesNegocio;
import negocioImpl.LocalidadesNegocioImpl;
import negocioImpl.PacientesNegocioImpl;
import negocioImpl.ProvinciasNegocioImpl;
import entidad.Provincia;
import excepciones.DniNoencontrado;
import excepciones.ErrorEliminacion;
import excepciones.ErrorEnInsertar;
import excepciones.ModificacionNoexitosa;
import entidad.Localidad;


/**
 * Servlet implementation class ServletsInsertarPacientes
 */
@WebServlet("/ServletsInsertarPacientes")
public class ServletsInsertarPacientes extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletsInsertarPacientes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//CARGAR DDLs
				if(request.getParameter("Param")!=null)
				{
					//Entra por haber hecho click en el hyperlink
			        ProvinciasNegocioImpl provneg = new ProvinciasNegocioImpl();
			        LocalidadesNegocioImpl localneg = new LocalidadesNegocioImpl();
			        
			        //carga ddl Provincias
			        List<Provincia> listaProv = provneg.readAllProvincias();
			        	//mando la lista al jsp
			        request.setAttribute("listaProv", listaProv);
			        
			        // carga ddl Localidades
			        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
		        		//mando la lista al jsp
			        request.setAttribute("listaLoc", listaLoc);
			        
			        //me vuelvo al jsp
			        RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioPac.jsp");
			        rd.forward(request, response);
			        return;
				}
		
		
		//BUSCAR//
        if (request.getParameter("btnBuscarPac") != null) {
            
        	ProvinciasNegocioImpl provneg2 = new ProvinciasNegocioImpl();
        	LocalidadesNegocioImpl localneg2 = new LocalidadesNegocioImpl();
        	PacientesNegocio pacneg = new PacientesNegocioImpl();
      
    	//VOLVER A CARGAR DDLs 
            //carga ddl Provincias
	        List<Provincia> listaProv = provneg2.readAllProvincias();
	        	//mando la lista al jsp
	        request.setAttribute("listaProv", listaProv);
	        
	        // carga ddl Localidades
	        List<Localidad> listaLoc = localneg2.readAllLocalidades(null);
        		//mando la lista al jsp
	        request.setAttribute("listaLoc", listaLoc);
            
            Pacientes pac = new Pacientes();
            boolean estado = false;
            String dni = "";
            String nombre = "";
            String apellido = "";
            char sexo = ' ';
            String nacionalidad = "";
            String fecha_nac = "";
            String direccion = "";
            String localidad = "";
            String provincia = "";
            String email = "";
            String telefono = "";
            if(!request.getParameter("txtDniUsuario").isEmpty())
            {  
                try {
                	String DNI = request.getParameter("txtDniUsuario");
                    pac = pacneg.BuscarPacientesPorDni(DNI);
                    if(pac==null) {
                    	
                     	 DniNoencontrado aux = new DniNoencontrado();
                     	 
                        throw aux;
                     
                        
                   }
                    else {
                    
                    dni = pac.getDNI_Pacientes();
                    nombre = pac.getNombre_Pacientes();
                    apellido = pac.getApellido_Pacientes();
                    sexo = pac.getSexo_Pacientes();
                    nacionalidad = pac.getNacionalidad_Pacientes();
                    fecha_nac = pac.getFecha_Nacimiento_Pacientes();
                    direccion = pac.getDireccion_Pacientes();
                    localidad = pac.getLocalidad();
                    provincia = pac.getProvincia();
                    email = pac.getEmail_Pacientes();
                    telefono = pac.getTelefono_Pacientes(); 
                    }
                   
                } catch (DniNoencontrado e) {
                	System.out.println("Se produjo una excepci�n: " + e.getMessage());
                    e.printStackTrace();
                    /*RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioPac.jsp");
                    rd.forward(request, response);
                    return;*/
                }
            }
            request.setAttribute("dni", dni);
            request.setAttribute("nombre", nombre);
            request.setAttribute("apellido", apellido);
            request.setAttribute("sexo", sexo);
            request.setAttribute("nacionalidad", nacionalidad);
            request.setAttribute("fecha_nac", fecha_nac);
            request.setAttribute("direccion", direccion);
            request.setAttribute("localidad", localidad);
            request.setAttribute("provincia", provincia);
            request.setAttribute("email", email);
            request.setAttribute("telefono", telefono);
            RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioPac.jsp");
            rd.forward(request, response);
        }
		
		
    //AGREGAR//
		if (request.getParameter("btnGuardarPac") != null) {
			PacientesNegocio pacneg2 = new PacientesNegocioImpl();
            Pacientes pac = new Pacientes();
            boolean estado = false;
            if(!request.getParameter("txtDni").isEmpty() && !request.getParameter("txtApellido").isEmpty() && 
            		!request.getParameter("txtNombre").isEmpty() && !request.getParameter("txtDireccion").isEmpty()
            		&& !request.getParameter("txtEmail").isEmpty() && !request.getParameter("trip-start").isEmpty() 
            		&& !request.getParameter("Localidades").isEmpty() && !request.getParameter("txtNacionalidad").isEmpty()
            		&& !request.getParameter("Provincias").isEmpty() && !request.getParameter("txtTelefono").isEmpty())
            {
            pac.setDNI_Pacientes(request.getParameter("txtDni"));
            pac.setApellido_Pacientes(request.getParameter("txtApellido"));
            pac.setNombre_Pacientes(request.getParameter("txtNombre"));
            pac.setDireccion_Pacientes(request.getParameter("txtDireccion"));
            pac.setEmail_Pacientes(request.getParameter("txtEmail"));
            pac.setFecha_Nacimiento_Pacientes(request.getParameter("trip-start"));
            pac.setLocalidad_nombreLocalidad(request.getParameter("Localidades"));
            pac.setNacionalidad_Pacientes(request.getParameter("txtNacionalidad"));
            pac.setProvincia_nombreProvincia(request.getParameter("Provincias"));
            pac.setSexo_Pacientes((request.getParameter("FemeninoMasculino")).charAt(0));
            pac.setTelefono_Pacientes(request.getParameter("txtTelefono"));
            
            System.out.println(pac.toString());

            try {
                  estado = pacneg2.insertPacientes(pac);
                  if(estado!=true) {
                      ErrorEnInsertar error= new ErrorEnInsertar();
                      throw error;
                  }

                } catch (ErrorEnInsertar  e) {
                	System.out.println("Se produjo una excepci�n: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            
          //Entra por haber hecho click en el hyperlink
	        ProvinciasNegocioImpl provneg = new ProvinciasNegocioImpl();
	        LocalidadesNegocioImpl localneg = new LocalidadesNegocioImpl();
	        
	        //carga ddl Provincias
	        List<Provincia> listaProv = provneg.readAllProvincias();
	        	//mando la lista al jsp
	        request.setAttribute("listaProv", listaProv);
	        
	        // carga ddl Localidades
	        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
        		//mando la lista al jsp
	        request.setAttribute("listaLoc", listaLoc);
	        
            request.setAttribute("Agrego", estado); 
            RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioPac.jsp");
            rd.forward(request, response);
        }
		
		
	//MODIFICAR//
        if (request.getParameter("btnModificarPac") != null) {
        	PacientesNegocio pacneg3 = new PacientesNegocioImpl();
            Pacientes pac = new Pacientes();
            boolean estado = false;
            if(!request.getParameter("txtDni").isEmpty() && !request.getParameter("txtApellido").isEmpty() && 
            		!request.getParameter("txtNombre").isEmpty() && !request.getParameter("txtDireccion").isEmpty()
            		&& !request.getParameter("txtEmail").isEmpty() && !request.getParameter("trip-start").isEmpty() 
            		&& !request.getParameter("Localidades").isEmpty() && !request.getParameter("txtNacionalidad").isEmpty()
            		&& !request.getParameter("Provincias").isEmpty() && !request.getParameter("txtTelefono").isEmpty())
            {
            	pac.setDNI_Pacientes(request.getParameter("txtDni"));
                pac.setApellido_Pacientes(request.getParameter("txtApellido"));
                pac.setNombre_Pacientes(request.getParameter("txtNombre"));
                pac.setDireccion_Pacientes(request.getParameter("txtDireccion"));
                pac.setEmail_Pacientes(request.getParameter("txtEmail"));
                pac.setFecha_Nacimiento_Pacientes(request.getParameter("trip-start"));
                pac.setLocalidad_nombreLocalidad(request.getParameter("Localidades"));
                pac.setNacionalidad_Pacientes(request.getParameter("txtNacionalidad"));
                pac.setProvincia_nombreProvincia(request.getParameter("Provincias"));
                pac.setSexo_Pacientes((request.getParameter("FemeninoMasculino")).charAt(0));
                pac.setTelefono_Pacientes(request.getParameter("txtTelefono"));
                
                try {
                    estado=pacneg3.ModificarPacientePorDni(pac);
                    if(estado!=true) {
                        ModificacionNoexitosa mod =new ModificacionNoexitosa();
                        throw mod;
                    }

                } catch ( ModificacionNoexitosa e) {
                	System.out.println("Se produjo una excepci�n: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            
          //Entra por haber hecho click en el hyperlink
	        ProvinciasNegocioImpl provneg = new ProvinciasNegocioImpl();
	        LocalidadesNegocioImpl localneg = new LocalidadesNegocioImpl();
	        
	        //carga ddl Provincias
	        List<Provincia> listaProv = provneg.readAllProvincias();
	        	//mando la lista al jsp
	        request.setAttribute("listaProv", listaProv);
	        
	        // carga ddl Localidades
	        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
        		//mando la lista al jsp
	        request.setAttribute("listaLoc", listaLoc);

            request.setAttribute("Modifico", estado); 
            RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioPac.jsp");
            rd.forward(request, response);
        }
		
		
		//ELIMINAR//
        if (request.getParameter("btnEliminarPac") != null) {
        	PacientesNegocio pacneg4 = new PacientesNegocioImpl();
            Pacientes pac = new Pacientes();
            boolean estado = false;
            if(!request.getParameter("txtDni").isEmpty())
            {
	            pac.setDNI_Pacientes(request.getParameter("txtDni"));

                
                try {
                    estado = pacneg4.borrarPacientes(pac);
                    if(estado!=true) {
                        ErrorEliminacion eli= new ErrorEliminacion();
                        throw eli;
                    }

                } catch (ErrorEliminacion e) {
                	System.out.println("Se produjo una excepci�n: " + e.getMessage());
                    e.printStackTrace();
                }
            }
                
              //Entra por haber hecho click en el hyperlink
    	        ProvinciasNegocioImpl provneg = new ProvinciasNegocioImpl();
    	        LocalidadesNegocioImpl localneg = new LocalidadesNegocioImpl();
    	        
    	        //carga ddl Provincias
    	        List<Provincia> listaProv = provneg.readAllProvincias();
    	        	//mando la lista al jsp
    	        request.setAttribute("listaProv", listaProv);
    	        
    	        // carga ddl Localidades
    	        List<Localidad> listaLoc = localneg.readAllLocalidades(null);
            		//mando la lista al jsp
    	        request.setAttribute("listaLoc", listaLoc);    

            request.setAttribute("Borro", estado); 
            RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioPac.jsp");
            rd.forward(request, response);
        }
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
