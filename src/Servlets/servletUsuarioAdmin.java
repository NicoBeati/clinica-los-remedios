package Servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import entidad.Especialidad;
import entidad.Especialistas;
import entidad.Usuarios;
import excepciones.DniNoencontrado;
import excepciones.ErrorEliminacion;
import excepciones.ErrorEnDatos;
import excepciones.ModificacionNoexitosa;
import negocio.UsuariosNegocio;
import negocioImpl.UsuariosNegocioImpl;


/**
 * Servlet implementation class servletUsuarioAdmin
 */
@WebServlet("/servletUsuarioAdmin")
public class servletUsuarioAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public servletUsuarioAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	//LISTAR//
    	String Mensaje = "";
    	if (request.getParameter("Param") != null) {
    		UsuariosNegocioImpl usuneg = new UsuariosNegocioImpl();
            List<Usuarios> ListaUsuarios = usuneg.readAllUsuarios();

            request.setAttribute("listaUsuarios", ListaUsuarios);

            RequestDispatcher rd = request.getRequestDispatcher("/ListarUsuarios.jsp");
            rd.forward(request, response);
        }
    	
    	//BUSCAR//
        if (request.getParameter("btnBuscarAdm") != null) {
        	UsuariosNegocioImpl usuneg2 = new UsuariosNegocioImpl();
            Usuarios usu = new Usuarios();
            boolean estado = false;
            String dni = "";
            String nombre = "";
            String email = "";
            String contrasenia = "";
            if(!request.getParameter("txtDniUsuario").isEmpty())
            { 
            	String DNI = request.getParameter("txtDniUsuario");
            	if(usuneg2.VerificarUsuarioPorDni(DNI)) {
                try {
                	
                    usu = usuneg2.BuscarAdminPorDni(DNI);
                    dni = usu.getDni();
                    nombre = usu.getUsuario();
                    email = usu.getEmail_Usuarios();
                    contrasenia = usu.getContrasena_Usuarios(); 
                    
                    request.setAttribute("dni", dni);
                    request.setAttribute("nombre", nombre);
                    request.setAttribute("email", email);
                    request.setAttribute("contrasenia", contrasenia);
                    RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
                    rd.forward(request, response);
		            if(usu==null){
		                        DniNoencontrado dniex=new DniNoencontrado();
		                        throw dniex;
		                    }

                    
                } catch (DniNoencontrado e) {
                	System.out.println("Se produjo una excepci�n: " + e.getMessage());

                    e.printStackTrace();
                }
                 
            	}else {
            		Mensaje = "No existe usuario con este dni";
            		request.setAttribute("MensajeErrorBuscar", Mensaje );
                	RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
                    rd.forward(request, response);
            		}
            }else {
            	Mensaje = "Ingrese un n�mero de dni ";
            	request.setAttribute("MensajeErrorBuscar", Mensaje );
            	RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
                rd.forward(request, response);
            }
           
        }
        //AGREGAR//
        if (request.getParameter("btnGuardarAdm") != null) {
        	UsuariosNegocioImpl usuneg3 = new UsuariosNegocioImpl();
            Usuarios usu = new Usuarios();
            boolean estado = false;
            if(((!request.getParameter("txtDni").isEmpty() && !request.getParameter("txtEmail").isEmpty() )
            		&& (!request.getParameter("txtNombre").isEmpty() && !request.getParameter("txtContrasenia").isEmpty()))
            		&& ((!request.getParameter("txtConfContrasenia").isEmpty() )&& 
            		(request.getParameter("txtContrasenia").equals(request.getParameter("txtConfContrasenia")))))
            {
            	usu.setDni(request.getParameter("txtDni"));
                usu.setEmail_Usuarios(request.getParameter("txtEmail"));
                usu.setUsuario(request.getParameter("txtNombre"));
                usu.setTipo_Usuarios('A');
                usu.setContrasena_Usuarios(request.getParameter("txtContrasenia"));
                
                try {
                	if(usuneg3.BuscarRegistroUsuarioPorDni(request.getParameter("txtDni"))==2) {
                		
                		//No hay ningun registro con ese dni
                		estado = usuneg3.insertUsuarios(usu);
                	    if(estado) {
                            Mensaje = "Usuario administrador creado con �xito.";
                        	request.setAttribute("MensajeExito", Mensaje);
                        }else {
                        	Mensaje = "Error al crear el usuario.";
                        	request.setAttribute("MensajeError", Mensaje);
                        	
                        }

                	}
                	else if(usuneg3.BuscarRegistroUsuarioPorDni(request.getParameter("txtDni"))==0) {
                		
                		//Existe un registro desactivado
                		Mensaje = "Existe un registro usuario desactivado";
                		request.setAttribute("MensajeError", Mensaje);
                		
                		
                		//hago el update //REVISAR PROCEDIMIENTO ALMACENADO
                		if(usuneg3.ModificarEstadoDeUsuario(usu.getDni(), usu.getUsuario(),usu.getEmail_Usuarios(), usu.getContrasena_Usuarios())) {
                			Mensaje = "Usuario administrador creado con �xito.";
                			request.setAttribute("MensajeExito", Mensaje);
                		}else {
                			Mensaje = "Error al crear el usuario. UPDATE";
                			request.setAttribute("MensajeError", Mensaje);
                			ErrorEnDatos datos= new ErrorEnDatos();
                            throw datos;
                		}
                		
                	}
                	else if(usuneg3.BuscarRegistroUsuarioPorDni(request.getParameter("txtDni"))==1) {
                		System.out.println("hay un registro activo");
                		Mensaje = "Usuario existente";
                		
                		request.setAttribute("MensajeError", Mensaje);
                		
                	}
                	
                	RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
                    rd.forward(request, response);
                    
                    
                	/*if(!usuneg3.existeUsuario(usu)) {
                		estado = usuneg3.insertUsuarios(usu);
                	    if(estado) {
                            Mensaje = "Usuario administrador creado con �xito.";
                        	request.setAttribute("MensajeExito", Mensaje);
                        }else {
                        	
                        	Mensaje = "Error al crear el usuario.";
                        	request.setAttribute("MensajeError", Mensaje);
                        	
                        }

                	    RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
                        rd.forward(request, response);
                			
                	}else {
                		Mensaje = "Usuario ya existente.";
                		request.setAttribute("MensajeError", Mensaje);
                		RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
                        rd.forward(request, response);
                        ErrorEnDatos datos= new ErrorEnDatos();
                        throw datos;
                	}
                	*/
                   
                } catch (ErrorEnDatos e) {
                	System.out.println("Se produjo una excepci�n: " + e.getMessage());
                    e.printStackTrace();
                }
                
          }else {
            	
            	if(request.getParameter("txtContrasenia").equals(request.getParameter("txtConfContrasenia"))==false){
            		Mensaje = "Las contrase�as no coinciden. Por favor, verifique.";
            		request.setAttribute("MensajeError", Mensaje);
            		RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
                    rd.forward(request, response);
            	}
            	Mensaje = "Complete todos los campos.";
            	request.setAttribute("MensajeError", Mensaje);
            	
            }
            

            //request.setAttribute("Agrego", estado); 
            RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
            rd.forward(request, response);
        }
        //MODIFICAR//
        if (request.getParameter("btnModificarAdm") != null) {
        	UsuariosNegocio usuneg4 = new UsuariosNegocioImpl();
            Usuarios usu = new Usuarios();
            boolean estado = false;
            if(!request.getParameter("txtDni").isEmpty() && !request.getParameter("txtEmail").isEmpty() && 
            		!request.getParameter("txtNombre").isEmpty() && !request.getParameter("txtContrasenia").isEmpty()
            		&& !request.getParameter("txtConfContrasenia").isEmpty() && 
            		request.getParameter("txtContrasenia").equals(request.getParameter("txtConfContrasenia")))
            {
            	usu.setDni(request.getParameter("txtDni"));
                usu.setEmail_Usuarios(request.getParameter("txtEmail"));
                usu.setUsuario(request.getParameter("txtNombre"));
                usu.setTipo_Usuarios('A');
                usu.setContrasena_Usuarios(request.getParameter("txtContrasenia"));

                
                try {
                    estado=usuneg4.ModificarAdminPorDni(usu);
                    ModificacionNoexitosa mod= new ModificacionNoexitosa();
                    throw mod;
                } catch (ModificacionNoexitosa e) {
                	System.out.println("Se produjo una excepci�n: " + e.getMessage());
                    e.printStackTrace();
                }
            }
            if(estado) {
            	Mensaje = "Usuario administrador modificado con �xito.";
                request.setAttribute("MensajeExito", Mensaje);
            }else {
            	Mensaje = "Error al modificar el usuario.";
            	  request.setAttribute("MensajeError", Mensaje);
            }
            
        
            RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
            rd.forward(request, response);
        }
        
        //ELIMINAR//
        if (request.getParameter("btnEliminarAdm") != null) {
        	UsuariosNegocio usuneg5 = new UsuariosNegocioImpl();
            Usuarios usu = new Usuarios();
            boolean estado = false;
            if(!request.getParameter("txtDni").isEmpty() && !request.getParameter("txtEmail").isEmpty() && 
            		!request.getParameter("txtNombre").isEmpty() && !request.getParameter("txtContrasenia").isEmpty()
            		&& !request.getParameter("txtConfContrasenia").isEmpty() && 
            		request.getParameter("txtContrasenia").equals(request.getParameter("txtConfContrasenia")))
            {
            	usu.setDni(request.getParameter("txtDni"));
                usu.setEmail_Usuarios(request.getParameter("txtEmail"));
                usu.setUsuario(request.getParameter("txtNombre"));
                usu.setTipo_Usuarios('A');
                usu.setContrasena_Usuarios(request.getParameter("txtContrasenia"));

                if(usuneg5.BuscarRegistroUsuarioPorDni(request.getParameter("txtDni"))==1) {
                	//verifico que el usuario este activo
                try {
                    estado = usuneg5.borrarUsuarios(usu);
                    ErrorEliminacion eli= new ErrorEliminacion();
                    throw eli;
                } catch (ErrorEliminacion e) {
                	System.out.println("Se produjo una excepci�n: " + e.getMessage());
                    e.printStackTrace();
                }
                
            }}
            
            if(estado) {
             	Mensaje = "Usuario administrador eliminado con �xito.";
                request.setAttribute("MensajeExito", Mensaje);
            }else {
            	Mensaje = "Error al eliminar el usuario.";
          	  	request.setAttribute("MensajeError", Mensaje);
            }

            RequestDispatcher rd = request.getRequestDispatcher("/CrearModificarUsuarioAdmin.jsp");
            rd.forward(request, response);
        }
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
