package Servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import entidad.Turnos;
import excepciones.ErrorEliminacion;
import excepciones.ErrorEnInsertar;
import negocioImpl.TurnosNegocioImpl;


@WebServlet("/servletVistaMedico")
public class servletVistaMedico extends HttpServlet {
    private TurnosNegocioImpl turneg;

    public void init() throws ServletException {
        turneg = new TurnosNegocioImpl();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String usuarioMedico = (String) request.getSession().getAttribute("UsuarioMedico");
        if (usuarioMedico != null) {
            List<Turnos> turnosList = turneg.readAllTurnosPorDniEspecialista(usuarioMedico);
            request.setAttribute("turnosList", turnosList);
            request.getRequestDispatcher("/VistaMedico.jsp").forward(request, response);
        } else {
            // Si no se encuentra el valor en la sesi�n, redirige a otra p�gina o muestra un mensaje de error
            response.sendRedirect("error.jsp"); // Cambia "error.jsp" por la p�gina que desees mostrar en caso de no encontrar el valor
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String botonModificar = request.getParameter("modificar");
        boolean estado=false;

        if (botonModificar != null && botonModificar.equals("Modificar")) {
            String dniEspecialista = (String) request.getSession().getAttribute("UsuarioMedico");
            String codigoTurno = request.getParameter("codigoTurno");
            String motivoTurno = request.getParameter("motivoTurno");
            String estadoTurno = request.getParameter("estadoTurno");

            // Llama a la funci�n modificarTurno y pasa los par�metros necesarios
           
            
            try {
            	estado=turneg.modificarTurno(dniEspecialista, Integer.parseInt(codigoTurno), motivoTurno, Integer.parseInt(estadoTurno));
                if(estado!=true) {
                    ErrorEnInsertar error= new ErrorEnInsertar();
                    throw error;
                }

            } catch (ErrorEnInsertar e) {
            	System.out.println("Se produjo una excepci�n: " + e.getMessage());
                e.printStackTrace();
            }
            // Redirige al servletVistaMedico
            doGet(request, response);
        }
    }
}
